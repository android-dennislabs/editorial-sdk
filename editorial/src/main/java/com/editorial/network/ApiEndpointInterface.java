package com.editorial.network;


import com.editorial.model.TranslaterBean;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Amit on 1/10/2017.
 */

public interface ApiEndpointInterface {

    @GET
    Call<TranslaterBean> customUrlTranslate(@Url String url);

    @GET("translate")
    Call<TranslaterBean> wordTranslate(@Query("key") String key, @Query("text") String text
            , @Query("lang") String lang, @Query("format") String format, @Query("options") String options);
}
