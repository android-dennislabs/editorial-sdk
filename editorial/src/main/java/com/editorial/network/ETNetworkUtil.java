package com.editorial.network;

import android.content.Context;

import android.os.Handler;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.widget.Toast;

import com.config.config.ApiEndPoint;
import com.config.config.ConfigConstant;
import com.config.config.ConfigManager;
import com.config.util.ConfigUtil;
import com.editorial.EditorialSdk;
import com.editorial.listeners.EditorialCallback;
import com.editorial.model.ETCatBean;
import com.editorial.model.ETCategoryBean;
import com.editorial.model.ETCategoryProperty;
import com.editorial.model.ETServerBean;
import com.editorial.model.TranslaterBean;
import com.editorial.util.ETApiEndPoint;
import com.editorial.util.ETConstant;
import com.editorial.util.ETPreferences;
import com.editorial.util.EditorialUtil;
import com.editorial.util.database.ETDbHelper;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.helper.task.TaskRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ETNetworkUtil {

    private final ETDbHelper dbHelper;
    private final Context context;

    public ETNetworkUtil(Context activity) {
        this.context = activity;
        this.dbHelper = EditorialSdk.getInstance().getDBObject(activity);
    }

    public void fetchCategoryListFragment(ETCategoryProperty categoryProperty, int catId, int image, EditorialCallback.CategoryCallback listener) {
        isFirstHitCategory = true;
        new DataFromDB(categoryProperty, null, catId, image, listener).execute();
    }

    class DataFromDB{

        private final int catId;
        private final EditorialCallback.CategoryCallback listener;
        private final int image;
        private final ETCategoryProperty categoryProperty;
        private List<ETCatBean> catBeans;
        private ArrayList<ETCategoryBean> categoryBeen;
        private SparseArray<String> hashMapCategoryNames;
        private SparseArray<String> hashMapCategoryImages;

        public DataFromDB(ETCategoryProperty categoryProperty, List<ETCatBean> catBeans, int catId, int image, EditorialCallback.CategoryCallback listener) {
            this.categoryProperty = categoryProperty;
            this.catBeans = catBeans;
            this.catId = catId;
            this.image = image;
            this.listener = listener;
        }

        public void execute() {
            TaskRunner.getInstance().executeAsync(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.callDBFunction(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            if (catBeans != null && catBeans.size() > 0) {
                                dbHelper.insertCat(catBeans);
                            }
                            ArrayList<ETCategoryBean> list = dbHelper.fetchCategoryData(catId, categoryProperty.getImageRes(catId), image);
                            if (list != null && list.size() > 0) {
                                categoryBeen = list;
                                hashMapCategoryNames = new SparseArray<>(categoryBeen.size());
                                hashMapCategoryImages = new SparseArray<>(categoryBeen.size());
                                for (ETCategoryBean beanData : categoryBeen) {
                                    hashMapCategoryNames.put(beanData.getCategoryId(), beanData.getCategoryName());
                                    if (!TextUtils.isEmpty(beanData.getImageUrl())) {
                                        hashMapCategoryImages.put(beanData.getCategoryId(), beanData.getImageUrl());
                                    }
                                }
                            }
                            return null;
                        }
                    });
                    return null;
                }
            }, new TaskRunner.Callback<Void>() {
                @Override
                public void onComplete(Void result) {
                    if (categoryBeen != null && categoryBeen.size() > 0) {
                        listener.onCategoryLoaded(categoryBeen, hashMapCategoryNames, hashMapCategoryImages);
                    } else {
                        listener.onFailure(new Exception("No data"));
                    }
                    handleCat(categoryProperty, catId, image, categoryProperty.getHost(), listener);
                }
            });
        }
    }

    private boolean isFirstHitCategory = true;

    private void handleCat(final ETCategoryProperty categoryProperty, final int catId, final int image, String host, final EditorialCallback.CategoryCallback listener) {
        if (isFirstHitCategory) {
            isFirstHitCategory = false;
            Map<String, String> map = new HashMap<>(2);
            map.put("id", context.getPackageName());
            map.put("parent_id", catId + "");
            EditorialSdk.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, host, ApiEndPoint.GET_HOME_DATA, map, new ConfigManager.OnNetworkCall() {
                @Override
                public void onComplete(boolean status, String data) {
                    if (status && !EditorialUtil.isEmptyOrNull(data)) {
                        try {
                            final List<ETCatBean> list = ConfigManager.getGson()
                                    .fromJson(data, new TypeToken<List<ETCatBean>>() {
                                    }.getType());
                            if (list != null && list.size() > 0) {
                                dbHelper.callDBFunction(new Callable<Void>() {
                                    @Override
                                    public Void call() throws Exception {
                                        new DataFromDB(categoryProperty, list, catId, image, listener).execute();
                                        return null;
                                    }
                                });
                            }
                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }

    }


    public void downloadArticle(String id, final int catId, String host, final EditorialCallback.OnArticleResponse onCustomResponse) {
        Map<String, String> map = new HashMap<>(1);
        map.put("id", id);
        EditorialSdk.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, host,
                ETApiEndPoint.GET_CONTENT_BY_ID_OLD_FORMAT, map, new ConfigManager.OnNetworkCall() {
                    @Override
                    public void onComplete(boolean status, String data) {
                        if (status && !EditorialUtil.isEmptyOrNull(data)) {

                            try {
                                final ETServerBean serverBean = ConfigManager.getGson().fromJson(data, ETServerBean.class);
                                if (serverBean != null) {
                                    final List<ETServerBean> serverBeen = new ArrayList<>(1);
                                    serverBeen.add(serverBean);
                                    final ETDbHelper dbHelper = EditorialSdk.getInstance().getDBObject(context);
                                    dbHelper.callDBFunction(new Callable<Void>() {
                                        @Override
                                        public Void call() throws Exception {
                                            dbHelper.insertEditorial(serverBeen, catId, false);
                                            if (onCustomResponse != null)
                                                onCustomResponse.onCustomResponse(true, serverBean);
                                            return null;
                                        }
                                    });

                                } else if (onCustomResponse != null)
                                    onCustomResponse.onCustomResponse(false, null);
                            } catch (JsonSyntaxException e) {
                                if (onCustomResponse != null)
                                    onCustomResponse.onCustomResponse(false, null);
                                e.printStackTrace();
                            }
                        } else if (onCustomResponse != null)
                            onCustomResponse.onCustomResponse(false, null);

                    }
                });
    }

    public static void getTranslateWord(final Context context, final String word, final EditorialCallback.WordListener listener) {
        getTranslateWord(context, word, null, listener);
    }

    public static void getTranslateWord(final Context context, final String word, final String languageType, final EditorialCallback.WordListener listener) {
        listener.onProgressUpdate(View.VISIBLE);
        String keyTrans = "";
        if (EditorialSdk.getInstance().getConfigManager() != null
                && EditorialSdk.getInstance().getConfigManager().getHostAlias() != null) {
            String hostWordTranslate = EditorialSdk.getInstance().getConfigManager().getHostAlias().get(ETConstant.HOST_WORD_TRANSLATE);
            if(!TextUtils.isEmpty(hostWordTranslate) && hostWordTranslate.contains(ETConstant.YANDEX_TRANSLATE)) {
                keyTrans = ETPreferences.getKeyTrans(context);
            }
        }
        Call<TranslaterBean> call =EditorialSdk.getInstance().getWordTranslatorApiEndpointInterface(context).wordTranslate(
                keyTrans, word, TextUtils.isEmpty(languageType) ? getLanguageType(word) : languageType,"plain","1"
        );
        call.enqueue(new Callback<TranslaterBean>() {
            @Override
            public void onResponse(Call<TranslaterBean> call, Response<TranslaterBean> response) {
                try {
                    listener.onProgressUpdate(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (response.body() != null && response.body().getCode() == 200
                        && response.body().getText() != null && response.body().getText().size() > 0
                        && !ConfigUtil.isEmptyOrNull(response.body().getText().get(0))) {
                    listener.onTranslate(word, response.body().getText().get(0));
                } else {
                    Toast.makeText(context, "Error, please try later.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TranslaterBean> call, Throwable t) {
                try {
                    listener.onProgressUpdate(View.GONE);
                    listener.onError(new Exception("Error, please try later."));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Toast.makeText(context, "Error, please try later.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private static String getLanguageType(String word) {
        return EditorialUtil.isWordInHindiLanguage(word) ? ETConstant.TRANS_TYPE_HIN : ETConstant.TRANS_TYPE_ENG;
    }

}
