package com.editorial.model;

/**
 * Created by Amit on 3/11/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TranslaterBean {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("detected")
    @Expose
    private Detected detected;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("text")
    @Expose
    private List<String> text = null;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Detected getDetected() {
        return detected;
    }

    public void setDetected(Detected detected) {
        this.detected = detected;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public List<String> getText() {
        return text;
    }

    public void setText(List<String> text) {
        this.text = text;
    }

}


