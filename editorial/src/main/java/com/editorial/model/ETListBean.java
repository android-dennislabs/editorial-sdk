package com.editorial.model;

import com.adssdk.BaseAdModelClass;

import java.io.Serializable;

/**
 * Created by Amit on 2/23/2017.
 */

public class ETListBean extends BaseAdModelClass implements Serializable {

    private int subCatId , id ;
    private String text , date , tag, category;
    private boolean isTrue , isFav ;

    public boolean isFav() {
        return isFav;
    }

    public void setFav(boolean fav) {
        isFav = fav;
    }


    public int getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(int subCatId) {
        this.subCatId = subCatId;
    }

    public int getId() {
        return id;
    }

    public ETListBean setId(int id) {
        this.id = id;
        return this;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isTrue() {
        return isTrue;
    }

    public void setTrue(boolean aTrue) {
        isTrue = aTrue;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
