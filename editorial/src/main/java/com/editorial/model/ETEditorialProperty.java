package com.editorial.model;

import com.editorial.util.ETLogger;

import java.io.Serializable;

public class ETEditorialProperty implements Serializable, Cloneable {
    private ETEditorialBean editorial;
    private ETCategoryProperty categoryProperty;

    public ETEditorialBean getEditorial() {
        return editorial;
    }

    public ETEditorialProperty setEditorial(ETEditorialBean editorial) {
        this.editorial = editorial;
        return this;
    }

    public ETCategoryProperty getCategoryProperty() {
        return categoryProperty;
    }

    public ETEditorialProperty setCategoryProperty(ETCategoryProperty categoryProperty) {
        this.categoryProperty = categoryProperty;
        return this;
    }

    public static ETEditorialProperty Builder() {
        return new ETEditorialProperty();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public ETEditorialProperty getClone() {
        try {
            return (ETEditorialProperty) clone();
        } catch (CloneNotSupportedException e) {
            ETLogger.d(ETLogger.getClassPath(this.getClass(),"getClone"),e.toString());
            return new ETEditorialProperty();
        }
    }


}
