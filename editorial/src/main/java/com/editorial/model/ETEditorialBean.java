package com.editorial.model;

import com.adssdk.BaseAdModelClass;

import java.io.Serializable;

/**
 * Created by Amit on 2/23/2017.
 */

public class ETEditorialBean extends BaseAdModelClass implements Serializable {

    private int id, catId, subCatId;
    private String title, desc, readDetails, applyOnline, date, tag, category;
    private boolean isTrue, isRead, isFav;

    public int getId() {
        return id;
    }

    public ETEditorialBean setId(int id) {
        this.id = id;
        return this;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public int getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(int subCatId) {
        this.subCatId = subCatId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getReadDetails() {
        return readDetails;
    }

    public void setReadDetails(String readDetails) {
        this.readDetails = readDetails;
    }

    public String getApplyOnline() {
        return applyOnline;
    }

    public void setApplyOnline(String applyOnline) {
        this.applyOnline = applyOnline;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isTrue() {
        return isTrue;
    }

    public void setTrue(boolean aTrue) {
        isTrue = aTrue;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public boolean isFav() {
        return isFav;
    }

    public void setFav(boolean fav) {
        isFav = fav;
    }
}
