package com.editorial.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amit on 6/30/2017.
 */

public class ETCatBean {

    public ETCatBean(String title, int id, int parent_id) {
        this.title = title;
        this.id = id;
        this.parent_id = parent_id;
    }

    public ETCatBean() {
    }

    @SerializedName("title")
    @Expose
    private String title ;
    @SerializedName("image")
    @Expose
    private String image ;

    @SerializedName("id")
    @Expose
    private int id ;

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @SerializedName("parent_id")
    @Expose
    private int parent_id ;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
