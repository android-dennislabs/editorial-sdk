package com.editorial.model;

import com.adssdk.BaseAdModelClass;

import java.io.Serializable;

/**
 * Created by Amit on 2/23/2017.
 */

public class ETVocubModel extends BaseAdModelClass implements Serializable {

    private int id;
    private String word, meaning, date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
