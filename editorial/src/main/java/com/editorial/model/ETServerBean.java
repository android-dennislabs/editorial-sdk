package com.editorial.model;

/**
 * Created by Amit on 1/30/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ETServerBean extends ETServerBeanArticle {

    @SerializedName("pdf")
    @Expose
    private String pdf;
    @SerializedName(value="option1", alternate={"option_A"})
    @Expose
    private String optionA;

    public String getOptionC() {
        return optionC;
    }

    public void setOptionC(String optionC) {
        this.optionC = optionC;
    }

    @SerializedName(value="option3", alternate={"option_C"})
    @Expose
    private String optionC;
    @SerializedName(value="option2", alternate={"option_B"})
    @Expose
    private String optionB;

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getOptionA() {
        return optionA;
    }

    public void setOptionA(String optionA) {
        this.optionA = optionA;
    }

    public String getOptionB() {
        return optionB;
    }

    public void setOptionB(String optionB) {
        this.optionB = optionB;
    }

}