package com.editorial.model;

import com.editorial.util.ETLogger;

import java.io.Serializable;
import java.util.HashMap;


public class ETCategoryProperty implements Serializable, Cloneable {


    private String title = "", imageUrl = "", host = "", query = "";
    private int catId = 0, subCatId = 0, imageResId = 0, type = 0;
    private boolean isDate = false,isSubCat = false, isNotification = false, isLoadUI = false;
    private HashMap<Integer, int[]> imageRes;

    public ETCategoryProperty() {
    }

    public String getQuery() {
        return query;
    }

    public ETCategoryProperty setQuery(String query) {
        this.query = query;
        return this;
    }

    public boolean isDate() {
        return isDate;
    }

    public ETCategoryProperty setDate(boolean date) {
        isDate = date;
        return this ;
    }

    public boolean isLoadUI() {
        return isLoadUI;
    }

    public ETCategoryProperty setLoadUI(boolean loadUI) {
        isLoadUI = loadUI;
        return this;
    }

    public int[] getImageRes(int catId) {
        int[] images = null;
        if(imageRes!=null){
            images = imageRes.get(catId);
        }
        return images;
    }

    public ETCategoryProperty setImageRes(HashMap<Integer, int[]> imageRes) {
        this.imageRes = imageRes;
        return this;
    }

    public boolean isNotification() {
        return isNotification;
    }

    public ETCategoryProperty setNotification(boolean notification) {
        isNotification = notification;
        return this;
    }

    public int getSubCatId() {
        return subCatId;
    }

    public ETCategoryProperty setSubCatId(int subCatId) {
        this.subCatId = subCatId;
        return this;
    }


    public String getHost() {
        return host;
    }

    public ETCategoryProperty setHost(String host) {
        this.host = host;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public ETCategoryProperty setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public ETCategoryProperty setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public int getCatId() {
        return catId;
    }

    public ETCategoryProperty setCatId(int catId) {
        this.catId = catId;
        return this;
    }

    public int getImageResId() {
        return imageResId;
    }

    public ETCategoryProperty setImageResId(int imageResId) {
        this.imageResId = imageResId;
        return this;
    }

    public int getType() {
        return type;
    }

    public ETCategoryProperty setType(int type) {
        this.type = type;
        return this;
    }

    public boolean isSubCat() {
        return isSubCat;
    }

    public ETCategoryProperty setSubCat(boolean subCat) {
        isSubCat = subCat;
        return this;
    }

    public static ETCategoryProperty Builder() {
        return new ETCategoryProperty();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public ETCategoryProperty getClone() {
        try {
            return (ETCategoryProperty) clone();
        } catch (CloneNotSupportedException e) {
            ETLogger.d(ETLogger.getClassPath(this.getClass(),"getClone"),e.toString());
            return new ETCategoryProperty();
        }
    }
}
