package com.editorial.model;

/**
 * Created by Amit on 1/11/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ETIdBean {

    @SerializedName("id")
    @Expose
    private int id;


    @SerializedName("category_id")
    @Expose
    private int categoryId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}