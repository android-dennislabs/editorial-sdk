package com.editorial;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;

import com.adssdk.AdsSDK;
import com.config.config.ConfigManager;
import com.config.config.RetrofitGenerator;
import com.config.util.ConfigUtil;
import com.config.util.CryptoUtil;
import com.editorial.listeners.EditorialCallback;
import com.editorial.model.ETCategoryProperty;
import com.editorial.network.ApiEndpointInterface;
import com.editorial.util.ETConstant;
import com.editorial.util.ETTextToSpeechUtil;
import com.editorial.util.EditorialClassUtil;
import com.editorial.util.database.ETDbHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class EditorialSdk {

    private static EditorialSdk _instance;
    private final Picasso picasso;
    private AdsSDK adsSDK;
    private boolean isEnableDebugMode;
    private ETDbHelper dbHelper;
    private ConfigManager configManager;
    private Typeface typeface;
    private String deepLinkUrl;
    private final ArrayList<EditorialCallback.ShareListener> mShareListener = new ArrayList<>();

    public EditorialSdk() {
        _instance = this;
        picasso = Picasso.get();
    }

    public static void openPointsActivity(Context context) {
        EditorialClassUtil.openPointsActivity(context);
    }

    public static void openWordsActivity(Context context) {
        EditorialClassUtil.openWordsActivity(context);
    }

    public EditorialSdk initUtils(Context context){
        getTypeface(context);
        getTextSpeech(context);
        return this;
    }


    public Typeface getTypeface(Context context) {
        if(typeface == null){
            typeface = Typeface.createFromAsset(context.getAssets(), "fonts/tundra_web_bold_regular.ttf");
        }
        return typeface;
    }

    public EditorialSdk setTypeface(Typeface typeface) {
        this.typeface = typeface;
        return this;
    }

    public static EditorialSdk getInstance() {
        if (_instance == null) {
            _instance = new EditorialSdk();
        }
        return _instance;
    }


    public boolean isEnableDebugMode() {
        return isEnableDebugMode;
    }

    /**
     * @param isDebug = BuildConfig.DEBUG
     * @return this
     */
    public EditorialSdk setDebugMode(Boolean isDebug) {
        isEnableDebugMode = isDebug;
        return this;
    }


    public AdsSDK getAdsSDK() {
        if ( adsSDK == null ){
            adsSDK = AdsSDK.getInstance();
        }
        return adsSDK;
    }

    public EditorialSdk setAdsSDK(AdsSDK adsSDK) {
        this.adsSDK = adsSDK;
        return this;
    }

    public ETDbHelper getDBObject(Context context) {
        if (dbHelper == null) {
            dbHelper = ETDbHelper.getInstance(context);
        }
        return dbHelper;
    }

    public Picasso getPicasso() {
        return picasso;
    }

    private ETTextToSpeechUtil speechUtil;

    public ETTextToSpeechUtil getTextSpeech(Context context) {
        if(speechUtil == null){
            speechUtil = ETTextToSpeechUtil.getInstance(context);
        }
        return speechUtil;
    }


    public EditorialSdk setConfigManager(ConfigManager configManager) {
        this.configManager = configManager;
        return this;
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public ETCategoryProperty property;

    public void openCategoryActivity(Context context, int catId, String title, int image, boolean isSubCat , boolean isDate, String host) {
        ETCategoryProperty property = new ETCategoryProperty()
                .setCatId(catId)
                .setTitle(title)
                .setHost(host)
                .setDate(isDate)
                .setImageResId(image)
                .setSubCat(isSubCat);
        Intent intent = new Intent(context, EditorialClassUtil.getCategoryClass());
        this.property = property;
        intent.putExtra(ETConstant.CAT_PROPERTY, property);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private ApiEndpointInterface apiWordTranslatorEndpointInterface;

    public ApiEndpointInterface getWordTranslatorApiEndpointInterface(Context context) {
        if (apiWordTranslatorEndpointInterface == null) {
            if (EditorialSdk.getInstance().getConfigManager() != null
                    && EditorialSdk.getInstance().getConfigManager().getHostAlias() != null) {
                String hostWordTranslate = EditorialSdk.getInstance().getConfigManager().getHostAlias().get(ETConstant.HOST_WORD_TRANSLATE);
                if (hostWordTranslate != null) {
                    apiWordTranslatorEndpointInterface = RetrofitGenerator.getClient(hostWordTranslate, ConfigUtil.getSecurityCode(context), CryptoUtil.getUuidEncrypt(context)).create(ApiEndpointInterface.class);;
                }
            }
        }
        return apiWordTranslatorEndpointInterface;
    }

    public String getDeepLinkUrl() {
        if(deepLinkUrl == null){
            deepLinkUrl = ETConstant.DEEP_LINK_URL;
        }
        return deepLinkUrl;
    }

    public EditorialSdk setDeepLinkUrl(String deepLinkUrl) {
        this.deepLinkUrl = deepLinkUrl;
        return this;
    }

    public EditorialSdk addShareListener(EditorialCallback.ShareListener callback) {
        synchronized (mShareListener) {
            mShareListener.add(callback);
        }
        return this;
    }

    public Boolean isShareListenerAvailable() {
        return mShareListener.size() > 0;
    }

    public void removeShareListener(EditorialCallback.ShareListener callback) {
        synchronized (mShareListener) {
            mShareListener.remove(callback);
        }
    }

    public void dispatchShareUpdated(int catId, int clickedId, String description) {
        for (EditorialCallback.ShareListener callback : mShareListener) {
            callback.onShareButtonClicked(catId, clickedId, description);
        }
    }

    public EditorialSdk.OnUrlClick getOnUrlClick() {
        return OnUrlClick;
    }

    public EditorialSdk setOnUrlClick(EditorialSdk.OnUrlClick onUrlClick) {
        OnUrlClick = onUrlClick;
        return this ;
    }

    private EditorialSdk.OnUrlClick OnUrlClick;
    public interface OnUrlClick {
        void onPDFUrlClick(Activity activity , int id , String title , String catTitle , String pdf , String pdfUrl );
        void onWebUrlClick( Activity activity , String title , String url );
    }

}
