package com.editorial.activity;


import android.os.Bundle;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adssdk.PageAdsAppCompactActivity;
import com.editorial.EditorialSdk;
import com.editorial.R;
import com.editorial.adapter.ETCategoryAdapter;
import com.editorial.model.ETCategoryBean;
import com.editorial.model.ETCategoryProperty;
import com.editorial.model.ETEditorialBean;
import com.editorial.util.ETConstant;
import com.editorial.util.EditorialClassUtil;
import com.editorial.util.EditorialUtil;
import com.editorial.util.database.ETDBConstant;
import com.editorial.util.database.ETDbHelper;
import com.helper.task.TaskRunner;

import java.util.ArrayList;
import java.util.concurrent.Callable;


/**
 * Created by Amit on 1/18/2017.
 */

public class ETSearchActivity extends PageAdsAppCompactActivity implements ETCategoryAdapter.OnClick, SearchView.OnQueryTextListener {

    private ETDbHelper dbHelper;
    private ETCategoryAdapter mAdapter;
    private ArrayList<ETEditorialBean> homeBeen = new ArrayList<>();
    private String catQuery = "";
    private String query;
    private int image;
    private ETCategoryProperty categoryProperty = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.et_activity_search);
        initData();
        setupToolbar();
        initCategoryNames();
        setupList();
        EditorialUtil.initAds((RelativeLayout) findViewById(R.id.ll_ad), this, ETConstant.ADS_BANNER);
    }

    SparseArray<String> hashMapCategoryNames = null;
    SparseArray<String> hashMapCategoryImages = null;

    private void initCategoryNames() {
        if (dbHelper != null) {
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    ArrayList<ETCategoryBean> categoryBeen = dbHelper.MockFetchCategoryData(ETDbHelper.COLUMN_CAT_ID + "=" + catID, categoryProperty.getImageRes(catID), image);
                    if (categoryBeen != null && categoryBeen.size() > 0) {
                        hashMapCategoryNames = new SparseArray<>();
                        hashMapCategoryImages = new SparseArray<>();
                        for (ETCategoryBean beanData : categoryBeen) {
                            hashMapCategoryNames.put(beanData.getCategoryId(), beanData.getCategoryName());
                            hashMapCategoryImages.put(beanData.getCategoryId(), beanData.getImageUrl());
                        }
                    }
                    return null;
                }
            });
        }
    }

    int catID = 0;

    private void initData() {
        dbHelper = EditorialSdk.getInstance().getDBObject(this);
        if (getIntent().getSerializableExtra(ETConstant.CAT_PROPERTY) != null && getIntent().getSerializableExtra(ETConstant.CAT_PROPERTY).getClass() == ETCategoryProperty.class) {
            categoryProperty = (ETCategoryProperty) getIntent().getSerializableExtra(ETConstant.CAT_PROPERTY);
            if(categoryProperty!=null){
                catID = categoryProperty.getCatId();
                image = categoryProperty.getImageResId();

                catQuery = ETDbHelper.COLUMN_CAT_ID + "=" + catID;
            }
        }
    }

    private void setupList() {
        RecyclerView mRecyclerView = findViewById(R.id.itemsRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ETCategoryAdapter(homeBeen, this, null, null, this, hashMapCategoryNames, hashMapCategoryImages);
        mAdapter.setImageRes(image);
        mAdapter.setCategoryProperty(categoryProperty);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onCustomItemClick(int position, String category, int type) {
        if (type == ETCategoryAdapter.OnClick.TYPE_ARTICLE) {
            ETEditorialBean bean = homeBeen.get(position);
            bean.setCategory(category);
            EditorialClassUtil.openEditorialActivity(this, bean, catID, ETDbHelper.COLUMN_ID + "=" + homeBeen.get(position).getId(), categoryProperty.getHost());
        } else {
            updateFavStatus(position);
        }
    }

    private void updateFavStatus(final int position) {
        final boolean status = homeBeen.get(position).isFav();
        homeBeen.get(position).setFav(!status);
        mAdapter.notifyDataSetChanged();
        EditorialUtil.showToastCentre(this, status ? " Removed " : " Saved ");
        dbHelper.callDBFunction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.updateArticle(homeBeen.get(position).getId(), ETDBConstant.COLUMN_FAV,
                        status ? ETDBConstant.INT_FALSE : ETDBConstant.INT_TRUE, catID);
                return null;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.et_menu_search_act, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
//        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        return true;
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Search");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (!EditorialUtil.isEmptyOrNull(newText)) {
            query = catQuery;
            if (!EditorialUtil.isEmptyOrNull(catQuery))
                query = catQuery + " AND ";
            query = query + ETDbHelper.COLUMN_TITLE + " LIKE '%" + newText + "%'";
            requestDataFromDB();
        }
        return false;
    }

    private void requestDataFromDB(){
        TaskRunner.getInstance().executeAsync(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                homeBeen.clear();
                dbHelper.callDBFunction(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        dbHelper.fetchHomeData(homeBeen, query, catID, true);
                        return null;
                    }
                });
                return null;
            }
        }, new TaskRunner.Callback<Void>() {
            @Override
            public void onComplete(Void result) {
                mAdapter.notifyDataSetChanged();
                if (homeBeen.size() == 0)
                    Toast.makeText(ETSearchActivity.this, "No Data Found", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
