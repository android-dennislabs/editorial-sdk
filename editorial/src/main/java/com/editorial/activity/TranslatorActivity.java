package com.editorial.activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.adssdk.PageAdsAppCompactActivity;
import com.editorial.R;
import com.editorial.util.ETConstant;
import com.editorial.util.EditorialUtil;
import com.editorial.util.WebViewSupport;


public class TranslatorActivity extends PageAdsAppCompactActivity  {


    private WebViewSupport webViewSupport;
    private String mWord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.et_activity_translator);
//        setupToolbar();
        initView();
        initArguments();
    }

    private void initArguments() {
        if (getIntent().getStringExtra(ETConstant.TRANS_WORD) != null) {
            mWord =  getIntent().getStringExtra(ETConstant.TRANS_WORD);
            loadDictionary(mWord);
        }else {
            EditorialUtil.showToastCentre(this,ETConstant.ERROR_INTEGRATION);
            finish();
        }
    }


    private void initView() {
        WebView webView = findViewById(R.id.webView);
        webViewSupport = new WebViewSupport(webView);
    }

    private void loadDictionary(String word) {
//        String url = "https://www.dictionary.com/browse/" + word;
        String url = EditorialUtil.getDictWebUrl(word , EditorialUtil.isWordInHindiLanguage(word) );
        webViewSupport.loadUrl(url);
    }

    protected void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        TextView tvTitle = findViewById(R.id.tv_title);
        View ivBack = findViewById(R.id.iv_back);
        tvTitle.setText("Vocabulary\n(Saved Word)");
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

}
