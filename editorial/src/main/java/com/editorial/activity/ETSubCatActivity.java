package com.editorial.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.adssdk.PageAdsAppCompactActivity;
import com.editorial.R;
import com.editorial.fragment.ETCategoryFragment;
import com.editorial.model.ETCategoryProperty;
import com.editorial.util.ETConstant;
import com.editorial.util.EditorialUtil;
import com.editorial.util.database.ETDBConstant;

/**
 * Created by Amit on 1/13/2017.
 */

public class ETSubCatActivity extends PageAdsAppCompactActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.et_activity_sub_cat);

        if (getIntent().getSerializableExtra(ETConstant.CAT_PROPERTY) != null
                && getIntent().getSerializableExtra(ETConstant.CAT_PROPERTY).getClass() == ETCategoryProperty.class) {
            ETCategoryProperty categoryProperty = (ETCategoryProperty) getIntent().getSerializableExtra(ETConstant.CAT_PROPERTY);
            if (categoryProperty != null) {

                int catID = categoryProperty.getCatId();
                int subCatID = categoryProperty.getSubCatId();
                int image = categoryProperty.getImageResId();
                String imageUrl = categoryProperty.getImageUrl();

                String title = categoryProperty.getTitle();
                setupToolbar(title);
                setCategoryData(catID, subCatID, image, categoryProperty, imageUrl);
                EditorialUtil.initAds((RelativeLayout) findViewById(R.id.ll_ad), this, ETConstant.ADS_BANNER);

            } else {
                EditorialUtil.showToastCentre(this, "Error, Something went wrong!");
                finish();
            }
        } else {
            EditorialUtil.showToastCentre(this, "Error, Something went wrong!");
            finish();
        }
    }

    private void setCategoryData(int catID, int subCatID, int image, ETCategoryProperty categoryProperty, String imageUrl) {
        if (catID != 0) {
            Fragment categoryFragment;
            categoryFragment = new ETCategoryFragment();
            Bundle bundle = new Bundle();
            ETCategoryProperty catProperty = categoryProperty.getClone()
                    .setCatId(catID)
                    .setSubCatId(subCatID)
                    .setQuery(ETDBConstant.COLUMN_SUB_CAT_ID + "=" + subCatID)
                    .setImageResId(image)
                    .setImageUrl(imageUrl)
                    .setLoadUI(true);

            bundle.putSerializable(ETConstant.CAT_PROPERTY, catProperty);
            categoryFragment.setArguments(bundle);

            getSupportFragmentManager().beginTransaction().add(R.id.frameLayout, categoryFragment).commit();
        }

    }


    private void setupToolbar(String title) {
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (!EditorialUtil.isEmptyOrNull(title)) {
                getSupportActionBar().setTitle(title);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
