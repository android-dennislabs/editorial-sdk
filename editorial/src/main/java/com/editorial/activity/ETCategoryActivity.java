package com.editorial.activity;

import android.content.Intent;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.adssdk.PageAdsAppCompactActivity;
import com.config.config.ApiEndPoint;
import com.config.config.ConfigConstant;
import com.config.config.ConfigManager;
import com.editorial.EditorialSdk;
import com.editorial.R;
import com.editorial.fragment.ETCategoryFragment;
import com.editorial.fragment.ETCategoryListFragment;
import com.editorial.fragment.ETBaseFragment;
import com.editorial.model.ETCategoryProperty;
import com.editorial.model.ETIdBean;
import com.editorial.util.ETConstant;
import com.editorial.util.ETPreferences;
import com.editorial.util.EditorialClassUtil;
import com.editorial.util.EditorialUtil;
import com.editorial.util.database.ETDBConstant;
import com.editorial.util.database.ETDbHelper;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.helper.task.TaskRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;


/**
 * Created by Amit on 1/12/2017.
 */

public class ETCategoryActivity extends PageAdsAppCompactActivity implements ViewPager.OnPageChangeListener {

    private int catID;
    private String title = "";
    private int image;
    private boolean isCategory, isNotification = false;
    private boolean isSecond = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.et_activity_category);
        initDataFromArg();
        setupToolbar();
        setupViewPager();
        EditorialUtil.initAds((RelativeLayout) findViewById(R.id.ll_ad), this, ETConstant.ADS_BANNER);
    }

    private ETCategoryProperty categoryProperty;

    private void initDataFromArg() {
        if (getIntent().getSerializableExtra(ETConstant.CAT_PROPERTY) != null) {
            categoryProperty = (ETCategoryProperty) getIntent().getSerializableExtra(ETConstant.CAT_PROPERTY);
            initObjects();
        } else {
            finish();
        }
        handleNotification();
    }


    private void initObjects() {
        catID = categoryProperty.getCatId();
        isCategory = categoryProperty.isSubCat();
        title = categoryProperty.getTitle();
        image = categoryProperty.getImageResId();
    }


    private void handleNotification() {
        isNotification = getIntent().getBooleanExtra(ETConstant.CATEGORY, false);
        if (isNotification) {
            int articleId = getIntent().getIntExtra(ETConstant.CLICK_ITEM_ARTICLE, 0);
            if (articleId != 0) {
                EditorialClassUtil.openEditorialActivity(this, articleId,catID, ETDBConstant.COLUMN_ID + "=" + articleId, categoryProperty.getHost());
            }
        }
    }

    private void setupViewPager() {
        final ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.addOnPageChangeListener(this);
        setupViewPager(viewPager);

        TabLayout tabLayout =  findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupToolbar() {
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private ViewPagerAdapter adapter;
    private ETCategoryFragment latest;
    private ETCategoryFragment favourite;


    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        latest = new ETCategoryFragment();
        favourite = new ETCategoryFragment();

        Bundle bundle = getCategoryBundle(true, false);

        latest.setArguments(bundle);
        adapter.addFrag(latest, "Latest");

        bundle = getCategoryBundle(false, false);
        if (isCategory) {
            ETCategoryListFragment categoryListFragment = new ETCategoryListFragment();
            categoryListFragment.setArguments(bundle);
            adapter.addFrag(categoryListFragment, "Category");
        }

        bundle = getCategoryBundle(false, true);
        favourite.setArguments(bundle);
        adapter.addFrag(favourite, "Fav");

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
    }

    private Bundle getCategoryBundle(boolean isLoadUi, boolean isFav) {
        Bundle bundle = new Bundle();
        ETCategoryProperty catProperty = EditorialClassUtil.getCategoryProperty(categoryProperty,catID)
                .setImageResId(image)
                .setNotification(isNotification)
                .setLoadUI(isLoadUi);
        if(!isFav){
            catProperty.setQuery(ETDBConstant.COLUMN_CAT_ID + "=" + catID);
        }else {
            String q = ETDBConstant.COLUMN_FAV + "=" + ETDBConstant.INT_TRUE
                    + " AND " + ETDBConstant.COLUMN_CAT_ID + "=" + catID;
            catProperty.setQuery(q);
        }
        bundle.putSerializable(ETConstant.CAT_PROPERTY, catProperty);
        return bundle;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (adapter != null) {
            Fragment fragment = adapter.getItem(position);
            if (fragment != null && fragment instanceof ETBaseFragment) {
                ((ETBaseFragment) fragment).onRefreshFragment();
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void updateFragmentList(boolean isFavTab) {
        if(isFavTab) {
            if (latest != null) {
                latest.loadData();
            }
        }
        if (favourite != null) {
            favourite.loadData();
        }
    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>(3);
        private final List<String> mFragmentTitleList = new ArrayList<>(3);

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.et_menu_search, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
//        else if (id == R.id.action_search) {
//            Intent intent = new Intent(this, ETSearchActivity.class);
//            ETCategoryProperty catProperty = EditorialClassUtil.getCategoryProperty(categoryProperty,catID)
//                    .setImageResId(image)
//                    .setNotification(isNotification);
//            intent.putExtra(ETConstant.CAT_PROPERTY, catProperty);
//            startActivity(intent);
//        }
        return super.onOptionsItemSelected(item);
    }

    public void fetchLatestIds(long id) {
//        boolean isCall = isGovt || (AppData.getInstance().getNetworkQue().get(catID) != null
//                && !AppData.getInstance().getNetworkQue().get(catID));
        boolean isCall = true;
        if (!isNotification && isCall) {
            Map<String, String> map = new HashMap<>(2);
            map.put("id", catID + "");
            map.put("max_content_id", id + "");

            EditorialSdk.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, categoryProperty.getHost()
                    , ApiEndPoint.GET_CONTENT_AND_SUB_CAT_IDS_BY_CAT_ID
                    , map, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            if (status && !EditorialUtil.isEmptyOrNull(data)) {
                                try {
                                    List<ETIdBean> list = ConfigManager.getGson().fromJson(data, new TypeToken<List<ETIdBean>>() {
                                    }.getType());
                                    if (list != null && list.size() > 0) {
                                        requestDataFromDB(list);
                                        if (isSecond) {
                                            isSecond = false;
                                            fetchLatestIds(list.get(list.size() - 1).getId());
                                        }
                                    }
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    });

        }
    }

    private void requestDataFromDB(final List<ETIdBean> idBeen){
        TaskRunner.getInstance().executeAsync(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                final ETDbHelper dbHelper = EditorialSdk.getInstance().getDBObject(ETCategoryActivity.this);
                dbHelper.callDBFunction(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        dbHelper.insertArticleId(idBeen, catID);
                        return null;
                    }
                });
                return null;
            }
        }, new TaskRunner.Callback<Void>() {
            @Override
            public void onComplete(Void result) {
                Fragment fragment = adapter.getItem(0);
                if (fragment != null && fragment instanceof ETCategoryFragment) {
                    ((ETCategoryFragment) fragment).callIdFunc();
                }
            }
        });
    }
}
