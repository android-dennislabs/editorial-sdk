package com.editorial.activity;


import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adssdk.PageAdsAppCompactActivity;
import com.editorial.EditorialSdk;
import com.editorial.R;
import com.editorial.adapter.ETWordsAdapter;
import com.editorial.model.ETVocubModel;
import com.editorial.util.ETConstant;
import com.editorial.util.EditorialUtil;
import com.editorial.util.database.ETDbHelper;
import com.helper.task.TaskRunner;

import java.util.ArrayList;
import java.util.concurrent.Callable;


public class WordsActivity extends PageAdsAppCompactActivity  {

    private ETDbHelper dbHelper;
    private RecyclerView mRecyclerView;
    private ETWordsAdapter mAdapter;
    private ArrayList<ETVocubModel> mList = new ArrayList<>();
    private View llNoData;
    private View pbLoader;
    private TextView tvNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.et_activity_points);
        setupToolbar();
        initView();
        setupList();
        initDataSet();
        EditorialUtil.initAds((RelativeLayout) findViewById(R.id.ll_ad), this, ETConstant.ADS_BANNER);
    }

    protected void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        TextView tvTitle = findViewById(R.id.tv_title);
        View ivBack = findViewById(R.id.iv_back);
        tvTitle.setText("Vocabulary\n(Saved Word)");
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initView() {
        mRecyclerView = findViewById(R.id.itemsRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(WordsActivity.this, RecyclerView.VERTICAL, false));
        llNoData = findViewById(R.id.ll_no_data);
        tvNoData = findViewById(R.id.tv_no_data);
        tvNoData.setTextColor(Color.WHITE);
        tvNoData.setText("Processing, Please wait...");
        pbLoader = findViewById(R.id.player_progressbar);
    }

    private void setupList() {
        mAdapter = new ETWordsAdapter(mList, new ETWordsAdapter.OnClick() {
            @Override
            public void onDelete(int position, ETVocubModel item) {
                deletePoint(position, item);
            }
        } , this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void deletePoint(final int position, final ETVocubModel item) {
        TaskRunner.getInstance().executeAsync(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.callDBFunction(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        dbHelper.deleteWord(item);
                        return null;
                    }
                });
                return null;
            }
        }, new TaskRunner.Callback<Void>() {
            @Override
            public void onComplete(Void result) {
                mList.remove(position);
                mAdapter.notifyItemRemoved(position);
                mAdapter.notifyItemRangeChanged(position, mList.size());
                if (mList.size() == 0) {
                    showNoDataViews();
                }
            }
        });
    }


    private void initDataSet() {
        dbHelper = EditorialSdk.getInstance().getDBObject(this);
        downloadDataFromDB();
    }

    private void downloadDataFromDB(){
        TaskRunner.getInstance().executeAsync(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.callDBFunction(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        dbHelper.fetchVocubData(mList);
                        return null;
                    }
                });
                return null;
            }
        }, new TaskRunner.Callback<Void>() {
            @Override
            public void onComplete(Void result) {
                if (mList != null) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            handleData();
                        }
                    }, 100);
                }
            }
        });
    }

    private void handleData() {
        if (mList.size() > 0 && mRecyclerView != null && mAdapter != null) {
            llNoData.setVisibility(View.GONE);
            mRecyclerView.getRecycledViewPool().clear();
            mRecyclerView.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.notifyDataSetChanged();
                }
            });
        } else {
            showNoDataViews();
        }
    }

    private void showNoDataViews() {
        EditorialUtil.showView(llNoData);
        EditorialUtil.showView(tvNoData);
        if (tvNoData != null) {
            if (EditorialUtil.isConnected(this)) {
                tvNoData.setText(ETConstant.NO_DATA);
            } else {
                tvNoData.setText(ETConstant.NO_INTERNET);
            }
        }
        EditorialUtil.hideView(pbLoader);
    }

}
