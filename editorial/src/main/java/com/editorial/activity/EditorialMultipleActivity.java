package com.editorial.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.adssdk.PageAdsAppCompactActivity;
import com.config.config.ConfigConstant;
import com.editorial.EditorialSdk;
import com.editorial.R;
import com.editorial.adapter.EditorialAdapter;
import com.editorial.listeners.EditorialCallback;
import com.editorial.listeners.EditorialMode;
import com.editorial.model.ETCategoryBean;
import com.editorial.model.ETCategoryProperty;
import com.editorial.model.ETEditorialBean;
import com.editorial.model.ETEditorialProperty;
import com.editorial.model.ETServerBean;
import com.editorial.network.ETNetworkUtil;
import com.editorial.util.ETConstant;
import com.editorial.util.ETLogger;
import com.editorial.util.ETPreferences;
import com.editorial.util.ETTextEditor;
import com.editorial.feature.EditorialBottomSheetBehaviour;
import com.editorial.util.EditorialUtil;
import com.editorial.util.database.ETDbHelper;
import com.helper.task.TaskRunner;
import com.helper.util.BaseUtil;

import java.util.ArrayList;
import java.util.concurrent.Callable;


public class EditorialMultipleActivity extends PageAdsAppCompactActivity implements ViewPager.OnPageChangeListener, EditorialCallback.FontListener, EditorialCallback.DayNightMode {

    private ETDbHelper dbHelper;
    private ArrayList<ETEditorialBean> mEditorial = new ArrayList<>();
    private int clickedId, catId;
    private boolean isDayMode;
    private String query = "";
    private boolean isNotification = false, isFirst = true;
    private View llNoData;
    private ETNetworkUtil etNetworkUtil;
    private ETCategoryProperty categoryProperty;
    private ETEditorialProperty editorialProperty;
    private ETEditorialBean mItem;
    private ETTextEditor editor;
    private boolean isViewerMode;
    private View contentMain;
    private ViewPager viewPager;
    private EditorialAdapter pagerAdapter;
    private int position;
    private SparseArray<String> hashMapCategoryNames;
    private Typeface typeface;
    private EditorialBottomSheetBehaviour mBottomSheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.et_activity_editorial_multiple);
        setupToolbar();
        initUtils();
        initView();
        initDataSet();

        if (!EditorialUtil.isConnected(this))
            EditorialUtil.showToastInternet(this);
        initReceiver();
        EditorialUtil.initAds((RelativeLayout) findViewById(R.id.ll_ad), this, ETConstant.ADS_BANNER);
    }

    private void initUtils() {
        editor = new ETTextEditor(this);
        etNetworkUtil = new ETNetworkUtil(this);
        typeface = EditorialSdk.getInstance().getTypeface(this);
    }

    private void initCategoryNames() {
        etNetworkUtil.fetchCategoryListFragment(categoryProperty, catId, 0, new EditorialCallback.CategoryCallback() {
            @Override
            public void onCategoryLoaded(ArrayList<ETCategoryBean> categoryBeen, SparseArray<String> categoryNames, SparseArray<String> categoryImages) {
                hashMapCategoryNames = categoryNames;
                if (pagerAdapter != null) {
                    pagerAdapter.setCategoryData(hashMapCategoryNames);
                    pagerAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Exception e) {
                ETLogger.e(e.toString());
            }
        });
    }

    private boolean isBroadcastReceiver = false;

    private void initReceiver() {
        if (isNotification) {
            registerReceiver(broadcastReceiver, new IntentFilter(getPackageName() + ConfigConstant.CONFIG_LOADED));
            isBroadcastReceiver = true;
        }
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mEditorial == null)
                downloadArticle();

            try {
                unregisterReceiver(broadcastReceiver);
                isBroadcastReceiver = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isBroadcastReceiver) {
            try {
                unregisterReceiver(broadcastReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Editorial");
        }
    }


    private void initView() {
        contentMain = findViewById(R.id.content_main);
        viewPager = findViewById(R.id.vp_detail);
        viewPager.addOnPageChangeListener(this);
        llNoData = findViewById(R.id.ll_no_data);
        llNoData.setVisibility(View.GONE);
        mBottomSheet = new EditorialBottomSheetBehaviour(this).initUi(getWindow().getDecorView().getRootView());
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mBottomSheet != null) {
            mBottomSheet.onStart();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        if (mBottomSheet != null) {
            mBottomSheet.onPause();
        }
    }

    @Override
    public void onBackPressed() {
        if(mBottomSheet != null && mBottomSheet.isClosed()){
            super.onBackPressed();
        }
    }

    private void setData() {
        if (mBottomSheet != null) {
            pagerAdapter = new EditorialAdapter(this, mEditorial, editor, hashMapCategoryNames, mBottomSheet.getWordListener(), textSelectionCallback);
            pagerAdapter.setDayMode(isDayMode);
            viewPager.setAdapter(pagerAdapter);
            viewPager.setCurrentItem(position);
            checkRead();
            checkFav();
            handleMode(ETPreferences.isDayMode(this));
            updateMenuItemsAndData(EditorialMode.NORMAL);
        }
    }



    private void initDataSet() {
        dbHelper = EditorialSdk.getInstance().getDBObject(this);
        isDayMode = ETPreferences.isDayMode(this);
        if (getIntent().getSerializableExtra(ETConstant.EDITORIAL_PROPERTY) != null) {
            editorialProperty = (ETEditorialProperty) getIntent().getSerializableExtra(ETConstant.EDITORIAL_PROPERTY);
            if (editorialProperty != null) {
                categoryProperty = editorialProperty.getCategoryProperty();
                mItem = editorialProperty.getEditorial();
                query = categoryProperty.getQuery();
                clickedId = mItem.getId();
                catId = categoryProperty.getCatId();
                isNotification = categoryProperty.isNotification();
                contentMain.setBackgroundColor(isDayMode ? Color.WHITE : Color.BLACK);
                initCategoryNames();
                if (catId == 0) {
                    isViewerMode = true;
                    mEditorial.add(mItem);
                    loadUi();
                } else {
                    isViewerMode = false;
                    requestDownloadDataFromDB();
                }
            }
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int index) {
        position = index;
        checkRead();
        checkFav();
    }

    private void checkRead() {
        TaskRunner.getInstance().executeAsync(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (!mEditorial.get(position).isRead()) {
                    dbHelper.callDBFunction(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            dbHelper.updateArticle(mEditorial.get(position).getId(), ETDbHelper.COLUMN_READ, ETDbHelper.INT_TRUE, catId);
                            return null;
                        }
                    });
                }
                return null;
            }
        });

    }


    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void requestDownloadDataFromDB(){
        TaskRunner.getInstance().executeAsync(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.callDBFunction(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        position = dbHelper.fetchDescData(mEditorial, query, clickedId, catId);
                        return null;
                    }
                });
                return null;
            }
        }, new TaskRunner.Callback<Void>() {
            @Override
            public void onComplete(Void result) {
                if (mEditorial != null && mEditorial.size() > 0) {
                    loadUi();
                } else if (isNotification && isFirst) {
                    isFirst = !isFirst;
                    downloadArticle();
                } else
                    showErrorMessage();
            }
        });
    }

    private void loadUi() {
        if (mEditorial != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setData();
                }
            }, 400);
        }
    }


    private void showErrorMessage() {
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
    }

    private void downloadArticle() {
        if (EditorialUtil.isConnected(this)) {
            try {
                llNoData.setVisibility(View.VISIBLE);
                BaseUtil.showDialog(this, "Downloading...", true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (EditorialSdk.getInstance().getConfigManager().isConfigLoaded()) {
                etNetworkUtil.downloadArticle(clickedId + "", catId, categoryProperty.getHost(), new EditorialCallback.OnArticleResponse() {
                    @Override
                    public void onCustomResponse(boolean result, ETServerBean serverBean) {
                        llNoData.setVisibility(View.GONE);
                        BaseUtil.hideDialog();
                        if (result)
                            requestDownloadDataFromDB();
                        else {
                            finish();
                        }
                    }
                });
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onIncreaseFontSize() {
        handleFontSize(ETPreferences.getFontSize(this) - 1);
    }

    @Override
    public void onDecreaseFontSize() {
        handleFontSize(ETPreferences.getFontSize(this) + 1);
    }

    private void handleFontSize(int fontSize) {
        ETPreferences.setFontSize(this, fontSize);
        updatePager();
    }

    private void updatePager() {
        try {
            pagerAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSwitchDayNightMode(boolean isDayMode) {
        handleMode(isDayMode);
    }

    private void handleMode(boolean flag) {
        ETPreferences.setDayMode(this, flag);
        isDayMode = flag;
        pagerAdapter.setDayMode(flag);
        updatePager();
    }

    private MenuItem menuCancel, menuAddPoint, menuShare, menuBookmark, menuDayNight, menuMenu, menuDelete;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.et_menu_editorial, menu);
        menuCancel = menu.findItem(R.id.action_cancel);
        menuAddPoint = menu.findItem(R.id.action_add_point);
        menuShare = menu.findItem(R.id.action_share);
        menuMenu = menu.findItem(R.id.action_more);
        menuBookmark = menu.findItem(R.id.action_bookmark);
        menuDayNight = menu.findItem(R.id.action_day_night);
        menuDelete = menu.findItem(R.id.action_delete);
        return true;
    }

    private EditorialMode mode = EditorialMode.NORMAL;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.action_cancel) {
            updateMenuItemsAndData(EditorialMode.NORMAL);
        } else if (id == R.id.action_add_point) {
            addNewNoteInDatabase(getCurrentTextViewText());
        } else if (id == R.id.action_share) {
            pagerAdapter.share(position, catId, getWindowManager());
        } else if (id == R.id.action_bookmark) {
            updateFavStatus();
        } else if (id == R.id.action_day_night) {
            editor.openDayNightModeDialog(this);
        } else if (id == R.id.action_take_note) {
            updateMenuItemsAndData(EditorialMode.TAKE_NOTE);

        } else if (id == R.id.action_read) {
            readEditorial(getCurrentTextView());
        } else if (id == R.id.action_text_size) {
            editor.openFontDialog(this);
        } else if (id == R.id.action_delete) {
            editor.deletePoint(this, mEditorial.get(position));
        }
        return super.onOptionsItemSelected(item);
    }


    public void readEditorial(TextView textView) {
        if(textView!=null) {
            String text = textView.getText().toString();
            EditorialSdk.getInstance().getTextSpeech(this).speakText(text);
        }
    }

    private CharSequence getCurrentTextViewText() {
        View view = viewPager.findViewWithTag("pager_item" + position);
        if (view != null && view.findViewById(R.id.tv_desc) != null && view.findViewById(R.id.tv_desc) instanceof TextView) {
            TextView tvEditorial = (TextView) view.findViewById(R.id.tv_desc);
            CharSequence sltText = ETTextEditor.getSelectedText(tvEditorial);
            tvEditorial.clearFocus();
            return sltText;
        }
        return null;
    }

    private TextView getCurrentTextView() {
        View view = viewPager.findViewWithTag("pager_item" + position);
        if (view != null && view.findViewById(R.id.tv_desc) != null && view.findViewById(R.id.tv_desc) instanceof TextView) {
            return (TextView) view.findViewById(R.id.tv_desc);
        }
        return null;
    }

    private void updateMenuItemsAndData(EditorialMode mode) {
        this.mode = mode;
        pagerAdapter.setEditorialMode(mode);
        if (mode == EditorialMode.NORMAL) {
            if (menuCancel != null) {
                menuCancel.setVisible(false);
            }
            if (menuAddPoint != null) {
                menuAddPoint.setVisible(false);
            }
            if (menuShare != null) {
                menuShare.setVisible(true);
            }
//            if (menuDayNight != null) {
//                menuDayNight.setVisible(true);
//            }
            if (menuMenu != null) {
                menuMenu.setVisible(true);
            }
            if (menuBookmark != null) {
                menuBookmark.setVisible(true);
            }
        } else {
            if (menuCancel != null) {
                menuCancel.setVisible(true);
            }
            if (menuAddPoint != null) {
                menuAddPoint.setVisible(true);
            }
            if (menuShare != null) {
                menuShare.setVisible(false);
            }
//            if (menuDayNight != null) {
//                menuDayNight.setVisible(false);
//            }
            if (menuMenu != null) {
                menuMenu.setVisible(false);
            }
            if (menuBookmark != null) {
                menuBookmark.setVisible(false);
            }
        }
        if (isViewerMode) {
            if (menuBookmark != null) {
                menuBookmark.setVisible(false);
            }
            if (menuDelete != null) {
                menuDelete.setVisible(true);
            }
        } else {
            if (menuDelete != null) {
                menuDelete.setVisible(false);
            }
        }
        updatePager();
    }

    private void checkFav() {
        boolean status = false;
        if(mEditorial != null && mEditorial.size() > position) {
            status = mEditorial.get(position).isFav();
        }
        if (menuBookmark != null) {
            menuBookmark.setIcon(status ? R.drawable.ic_bookmark_fill_lite : R.drawable.ic_bookmark_not_fill_lite);
        }
    }

    public void updateFavStatus() {
        if (dbHelper != null && mEditorial != null) {
            final boolean status = mEditorial.get(position).isFav();
            menuBookmark.setIcon(status ? R.drawable.ic_bookmark_not_fill_lite : R.drawable.ic_bookmark_fill_lite);
            mEditorial.get(position).setFav(!status);
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.updateArticle(mEditorial.get(position).getId(), dbHelper.COLUMN_FAV,
                            status ? dbHelper.INT_FALSE : dbHelper.INT_TRUE, catId);
                    return null;
                }
            });
        }
    }

    private EditorialCallback.SelectionCallback textSelectionCallback = new EditorialCallback.SelectionCallback() {
        @Override
        public void onAddPointClick(CharSequence word) {
            addNewNoteInDatabase(word);
        }

        @Override
        public void enableEditorMode() {
            updateMenuItemsAndData(EditorialMode.TAKE_NOTE);
            EditorialUtil.showDialog(EditorialMultipleActivity.this, "Add Point", "Point selection mode is enabled");
        }
    };

    private void addNewNoteInDatabase(final CharSequence word) {
        if (TextUtils.isEmpty(word)) {
            Toast.makeText(EditorialMultipleActivity.this, "Please select points", Toast.LENGTH_SHORT).show();
            return;
        }
        TaskRunner.getInstance().executeAsync(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.callDBFunction(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        dbHelper.insertPoint(mItem, mEditorial.get(position), word, catId);
                        return null;
                    }
                });
                return null;
            }
        }, new TaskRunner.Callback<Void>() {
            @Override
            public void onComplete(Void result) {
                Toast.makeText(EditorialMultipleActivity.this, "Point added successful.", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
