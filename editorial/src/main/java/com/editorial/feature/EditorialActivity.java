//package com.editorial.activity;
//
//
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.content.pm.ActivityInfo;
//import android.graphics.Color;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.os.Handler;
//import android.text.Html;
//import android.text.Spanned;
//import android.text.TextUtils;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.webkit.WebView;
//import android.widget.Button;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import androidx.annotation.NonNull;
//import androidx.appcompat.widget.Toolbar;
//
//import com.adssdk.PageAdsAppCompactActivity;
//import com.config.config.ConfigConstant;
//import com.editorial.EditorialSdk;
//import com.editorial.R;
//import com.editorial.listeners.EditorialCallback;
//import com.editorial.listeners.EditorialMode;
//import com.editorial.model.ETCategoryProperty;
//import com.editorial.model.ETEditorialBean;
//import com.editorial.model.ETEditorialProperty;
//import com.editorial.model.ETServerBean;
//import com.editorial.network.ETNetworkUtil;
//import com.editorial.tasks.DataInsertPoints;
//import com.editorial.tasks.DataInsertWord;
//import com.editorial.tasks.TaskDeletePoint;
//import com.editorial.util.ETConstant;
//import com.editorial.util.ETPreferences;
//import com.editorial.util.ETTextEditor;
//import com.editorial.util.EditorialUtil;
//import com.editorial.util.TextActionModeCallback;
//import com.editorial.util.WebViewSupport;
//import com.editorial.util.database.ETDBConstant;
//import com.editorial.util.database.ETDbHelper;
//import com.google.android.material.bottomsheet.BottomSheetBehavior;
//
//import java.util.concurrent.Callable;
//
//
//public class EditorialActivity extends PageAdsAppCompactActivity implements EditorialCallback.FontListener, EditorialCallback.DayNightMode {
//
//    private ETDbHelper dbHelper;
//    private ETEditorialBean mEditorial;
//    private int clickedId, catId;
//    private boolean isDayMode;
//    private View mainView;
//    private String query = "";
//    private RelativeLayout rlAds;
//    private boolean isNotification = false, isFirst = true;
//    private ProgressDialog progressDialog;
//    private View llNoData;
//    private ETNetworkUtil etNetworkUtil;
//    private ETCategoryProperty categoryProperty;
//    private ETEditorialProperty editorialProperty;
//    private TextView tvTitle, tvEditorial, tvCategory, tvTag, tvDate, tvTranslatedWord;
//    private ETEditorialBean mItem;
//    private WebView webView;
//    private WebViewSupport webViewSupport;
//    private BottomSheetBehavior sheetBehavior;
//    private String mWord, mMeaning;
//    private ETTextEditor editor;
//    private LinearLayout layoutEditorial;
//    private boolean isViewerMode;
//    private View contentMain;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.et_activity_editorial);
//        editor = new ETTextEditor(this);
//        setupToolbar();
//        etNetworkUtil = new ETNetworkUtil(this);
//        initView();
//        initDataSet();
//        EditorialUtil.initAds(rlAds, this, ETConstant.ADS_BANNER);
//
//        if (!EditorialUtil.isConnected(this))
//            EditorialUtil.showToastInternet(this);
//        initReceiver();
//    }
//
//    private boolean isBroadcastReceiver = false;
//
//    private void initReceiver() {
//        if (isNotification) {
//            registerReceiver(broadcastReceiver, new IntentFilter(getPackageName() + ConfigConstant.CONFIG_LOADED));
//            isBroadcastReceiver = true;
//        }
//    }
//
//    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if (mEditorial == null)
//                downloadArticle();
//
//            try {
//                unregisterReceiver(broadcastReceiver);
//                isBroadcastReceiver = false;
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    };
//
//    @Override
//    protected void onDestroy() {
//        EditorialSdk.getInstance().getTextSpeech(this).stop();
//        super.onDestroy();
//        if (isBroadcastReceiver) {
//            try {
//                unregisterReceiver(broadcastReceiver);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    private void setupToolbar() {
//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setTitle("Editorial");
//        }
//    }
//
//
//    private void initView() {
//        contentMain = findViewById(R.id.content_main);
//        layoutEditorial = findViewById(R.id.layout_editorial);
//        llNoData = findViewById(R.id.ll_no_data);
//        llNoData.setVisibility(View.GONE);
//        mainView = findViewById(R.id.content_main);
//        rlAds = (RelativeLayout) findViewById(R.id.ll_ad);
//        tvTitle = findViewById(R.id.tv_title);
//        tvCategory = findViewById(R.id.tv_category);
//        tvTag = findViewById(R.id.tv_tag);
//        tvDate = findViewById(R.id.tv_date);
//        tvEditorial = findViewById(R.id.tv_desc);
//        tvTranslatedWord = findViewById(R.id.tv_translated_word);
//        LinearLayout layoutBottomSheet = findViewById(R.id.bottom_sheet);
//        LinearLayout bottomSheetHeader = findViewById(R.id.bottom_sheet_header);
//        Button btnSaveWord = findViewById(R.id.btn_save_word);
//        webView = findViewById(R.id.webView);
//        webViewSupport = new WebViewSupport(webView);
//
//        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
//
//        bottomSheetHeader.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mWord == null) {
//                    showSliderErrorMessage();
//                    return;
//                }
//                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
//                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
////                    btnBottomSheet.setText("Close sheet");
//                } else {
//                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
////                    btnBottomSheet.setText("Expand sheet");
//                }
//            }
//        });
//
//        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                if (mWord == null) {
//                    if (newState == BottomSheetBehavior.STATE_DRAGGING) {
//                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                    }
//                    showSliderErrorMessage();
//                }
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//
//            }
//        });
//
//        btnSaveWord.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                addWordInDatabase();
//            }
//        });
//    }
//
//    private boolean isMessageVisible = false;
//
//    private void showSliderErrorMessage() {
//        if (!isMessageVisible) {
//            EditorialUtil.showToastCentre(EditorialActivity.this, "Please select any word.");
//            isMessageVisible = true;
//        }
//    }
//
//    private void initDataSet() {
//        dbHelper = EditorialSdk.getInstance().getDBObject(this);
//        isDayMode = true;
//        if (getIntent().getSerializableExtra(ETConstant.EDITORIAL_PROPERTY) != null) {
//            editorialProperty = (ETEditorialProperty) getIntent().getSerializableExtra(ETConstant.EDITORIAL_PROPERTY);
//            if (editorialProperty != null) {
//                categoryProperty = editorialProperty.getCategoryProperty();
//                mItem = editorialProperty.getEditorial();
//                query = categoryProperty.getQuery();
//                clickedId = mItem.getId();
//                catId = categoryProperty.getCatId();
//                isNotification = categoryProperty.isNotification();
//                mainView.setBackgroundColor(isDayMode ? Color.WHITE : Color.BLACK);
//                progressDialog = new ProgressDialog(this);
//                progressDialog.setMessage("Downloading...");
//                if (catId == 0) {
//                    isViewerMode = true;
//                    mEditorial = mItem;
//                    loadUi();
//                } else {
//                    isViewerMode = false;
//                    new DownloadDataFromDB().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                }
//            }
//        }
//
//    }
//
//
//    private class DownloadDataFromDB extends AsyncTask<Void, Void, Void> {
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            dbHelper.callDBFunction(new Callable<Void>() {
//                @Override
//                public Void call() throws Exception {
//                    query = ETDBConstant.COLUMN_ID + "=" + clickedId;
//                    mEditorial = dbHelper.fetchDescData(query, catId);
//                    return null;
//                }
//            });
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void aVoid) {
//            super.onPostExecute(aVoid);
//            if (mEditorial != null) {
//                loadUi();
//            } else if (isNotification && isFirst) {
//                isFirst = !isFirst;
//                downloadArticle();
//            } else
//                showErrorMessage();
//        }
//    }
//
//    private void loadUi() {
//        if (mEditorial != null) {
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    setHeaderData();
//                    updateMenuItemsAndData(EditorialMode.NORMAL);
//                }
//            }, 100);
//        }
//    }
//
//
//    private void showErrorMessage() {
//        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
//    }
//
//    private void downloadArticle() {
//        if (EditorialUtil.isConnected(this)) {
//            try {
//                llNoData.setVisibility(View.VISIBLE);
//                progressDialog.show();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            if (EditorialSdk.getInstance().getConfigManager().isConfigLoaded()) {
//                etNetworkUtil.downloadArticle(clickedId + "", catId, new EditorialCallback.OnArticleResponse() {
//                    @Override
//                    public void onCustomResponse(boolean result, ETServerBean serverBean) {
//                        llNoData.setVisibility(View.GONE);
//                        try {
//                            progressDialog.dismiss();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                        if (result)
//                            new DownloadDataFromDB().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                        else {
//                            finish();
//                        }
//                    }
//                });
//            }
//        }
//    }
//
//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//    }
//
//    @Override
//    public void onIncreaseFontSize() {
//        handleFontSize(ETPreferences.getFontSize(this) - 1);
//    }
//
//    @Override
//    public void onDecreaseFontSize() {
//        handleFontSize(ETPreferences.getFontSize(this) + 1);
//    }
//
//    private void handleFontSize(int fontSize) {
//        ETPreferences.setFontSize(this, fontSize);
//        updateEditorialData();
//    }
//
//    @Override
//    public void onSwitchDayNightMode(boolean isDayMode) {
//        handleMode(isDayMode);
//    }
//
//    private void handleMode(boolean flag) {
//        ETPreferences.setDayMode(this, flag);
//        isDayMode = flag;
//        contentMain.setBackgroundColor(flag ? Color.WHITE : Color.DKGRAY);
//        layoutEditorial.setBackgroundColor(flag ? Color.WHITE : Color.DKGRAY);
//        tvTitle.setTextColor(flag ? Color.DKGRAY : Color.WHITE);
//        tvCategory.setBackgroundResource(flag ? R.drawable.bg_et_item_category_lite : R.drawable.bg_et_item_category_dark);
//        updateEditorialData();
//    }
//
//    private MenuItem menuCancel, menuAddPoint, menuShare, menuBookmark, menuDayNight, menuMenu, menuDelete;
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.et_menu_editorial, menu);
//        menuCancel = menu.findItem(R.id.action_cancel);
//        menuAddPoint = menu.findItem(R.id.action_add_point);
//        menuShare = menu.findItem(R.id.action_share);
//        menuMenu = menu.findItem(R.id.action_more);
//        menuBookmark = menu.findItem(R.id.action_bookmark);
//        menuDayNight = menu.findItem(R.id.action_day_night);
//        menuDelete = menu.findItem(R.id.action_delete);
//        return true;
//    }
//
//    private EditorialMode mode = EditorialMode.NORMAL;
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == android.R.id.home) {
//            onBackPressed();
//            return true;
//        } else if (id == R.id.action_cancel) {
//            updateMenuItemsAndData(EditorialMode.NORMAL);
//        } else if (id == R.id.action_add_point) {
//            addNewNoteInDatabase(ETTextEditor.getSelectedText(tvEditorial));
//        } else if (id == R.id.action_share) {
//            editor.share(layoutEditorial, catId, mEditorial.getId(), mEditorial.getDesc(), getWindowManager());
//        } else if (id == R.id.action_bookmark) {
//            updateFavStatus();
//        } else if (id == R.id.action_day_night) {
//            editor.openDayNightModeDialog(this);
//        } else if (id == R.id.action_take_note) {
//            updateMenuItemsAndData(EditorialMode.TAKE_NOTE);
//
//        } else if (id == R.id.action_read) {
//            readEditorial(this);
//        } else if (id == R.id.action_text_size) {
//            editor.openFontDialog(this);
//        } else if (id == R.id.action_delete) {
//            editor.deletePoint(this, mEditorial);
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    private void readEditorial(Context context) {
//        EditorialSdk.getInstance().getTextSpeech(context).speakText(tvEditorial.getText().toString());
//    }
//
//
//    private void updateMenuItemsAndData(EditorialMode mode) {
//        this.mode = mode;
//        if (mode == EditorialMode.NORMAL) {
//            menuCancel.setVisible(false);
//            menuAddPoint.setVisible(false);
//            menuShare.setVisible(true);
//            menuDayNight.setVisible(true);
//            menuMenu.setVisible(true);
//            menuBookmark.setVisible(true);
//            updateEditorialData();
//        } else {
//            menuCancel.setVisible(true);
//            menuAddPoint.setVisible(true);
//            menuShare.setVisible(false);
//            menuDayNight.setVisible(false);
//            menuMenu.setVisible(false);
//            menuBookmark.setVisible(false);
//            setEditorialPointMode();
//        }
//        if (isViewerMode) {
//            menuBookmark.setVisible(false);
//            menuDelete.setVisible(true);
//        } else {
//            menuDelete.setVisible(false);
//        }
//    }
//
//    private void checkFav() {
//        boolean status = mEditorial.isFav();
//        if (menuBookmark != null) {
//            menuBookmark.setIcon(status ? R.drawable.ic_bookmark_fill : R.drawable.ic_bookmark_not_fill);
//        }
//    }
//
//    public void updateFavStatus() {
//        if (dbHelper != null && mEditorial != null) {
//            final boolean status = mEditorial.isFav();
//            menuBookmark.setIcon(status ? R.drawable.ic_bookmark_not_fill : R.drawable.ic_bookmark_fill);
//            mEditorial.setFav(!status);
//            dbHelper.callDBFunction(new Callable<Void>() {
//                @Override
//                public Void call() throws Exception {
//                    dbHelper.updateArticle(mEditorial.getId(), dbHelper.COLUMN_FAV,
//                            status ? dbHelper.INT_FALSE : dbHelper.INT_TRUE, catId);
//                    return null;
//                }
//            });
//        }
//    }
//
//    private void setHeaderData() {
//        tvTitle.setText(mEditorial.getTitle());
//        if (!TextUtils.isEmpty(mItem.getCategory())) {
//            tvCategory.setText(mItem.getCategory());
//        } else {
//            tvCategory.setVisibility(View.GONE);
//        }
//        if (!TextUtils.isEmpty(mItem.getTag())) {
//            Spanned tag = Html.fromHtml(mItem.getTag());
//            tvTag.setText(tag.toString().replaceAll("\\n", ""));
//        } else {
//            tvTag.setVisibility(View.GONE);
//        }
//        if (!TextUtils.isEmpty(mItem.getDate())) {
//            tvDate.setText(mItem.getDate());
//        } else {
//            tvDate.setVisibility(View.GONE);
//        }
//        checkFav();
//        handleMode(ETPreferences.isDayMode(this));
//    }
//
//    private void loadDictionary(String word) {
//        String url = "https://www.dictionary.com/browse/" + word;
//        webViewSupport.loadUrl(url);
//    }
//
//    private void updateEditorialData() {
//        tvEditorial.setVisibility(View.VISIBLE);
//        tvEditorial.setTextIsSelectable(false);
//
//        editor.setClickableString(mode, tvEditorial, mEditorial.getDesc(), isDayMode, new EditorialCallback.WordListener() {
//
//            @Override
//            public void showProgress() {
//
//            }
//
//            @Override
//            public void hideProgress() {
//
//            }
//
//            @Override
//            public void onWordClicked(String word, String meaning) {
//                mWord = word;
//                mMeaning = meaning;
//                //update slider view
//                loadDictionary(mWord);
//                tvTranslatedWord.setText(word + " = " + meaning);
//            }
//
//            @Override
//            public void onError(Exception e) {
//
//            }
//        }, textSelectionCallback);
//
//    }
//
//    private EditorialCallback.SelectionCallback textSelectionCallback = new EditorialCallback.SelectionCallback() {
//        @Override
//        public void onAddPointClick(CharSequence word) {
//            addNewNoteInDatabase(word);
//        }
//    };
//
//    private void addNewNoteInDatabase(CharSequence word) {
//        new DataInsertPoints(EditorialActivity.this, mItem, mEditorial, word, catId, new EditorialCallback.Response<String>() {
//            @Override
//            public void onSuccess(String response) {
//                Toast.makeText(EditorialActivity.this, "Point added successful.", Toast.LENGTH_SHORT).show();
//                tvEditorial.clearFocus();
//            }
//
//            @Override
//            public void onFailure(Exception e) {
//
//            }
//        }).execute();
//    }
//
//    private void addWordInDatabase() {
//        if (mWord != null && mMeaning != null)
//            new DataInsertWord(EditorialActivity.this, mWord, mMeaning, new EditorialCallback.Response<String>() {
//                @Override
//                public void onSuccess(String response) {
//                    Toast.makeText(EditorialActivity.this, mWord + " added in vocabulary.", Toast.LENGTH_SHORT).show();
//                    tvEditorial.clearFocus();
//                }
//
//                @Override
//                public void onFailure(Exception e) {
//
//                }
//            }).execute();
//    }
//
//    private void setEditorialPointMode() {
//        tvEditorial.setTextIsSelectable(true);
//        TextActionModeCallback callback = new TextActionModeCallback(this, tvEditorial, textSelectionCallback);
//        tvEditorial.setCustomSelectionActionModeCallback(callback);
//    }
//}
