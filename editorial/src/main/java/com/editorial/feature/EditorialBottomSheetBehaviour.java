package com.editorial.feature;

import android.app.Activity;
import android.graphics.Color;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.editorial.EditorialSdk;
import com.editorial.R;
import com.editorial.listeners.EditorialCallback;
import com.editorial.network.ETNetworkUtil;
import com.editorial.util.ETPreferences;
import com.editorial.util.EditorialUtil;
import com.editorial.util.WebViewSupport;
import com.editorial.util.database.ETDbHelper;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.helper.task.TaskRunner;

import java.util.concurrent.Callable;

public class EditorialBottomSheetBehaviour {

    private final Activity activity;
    private LinearLayout layoutBottomSheet, bottomSheetHeader;
    private BottomSheetBehavior sheetBehavior;
    private View llSwipeUpMessage;
    private View layoutBottomSheetBorder;
    private TextView tvTranslatedWord;
    private ImageView btnSpeaker;
    private ProgressBar pbMeaning;
    private View ivBack;
    private ETDbHelper dbHelper;

    private WebViewSupport webViewSupport;
    private String mWord, mMeaning;
    private static final long RESTORE_BOTTOM_BAR_AFTER_TIME_TAKEN = 5000;

    public EditorialBottomSheetBehaviour(Activity activity) {
        this.activity = activity;
    }

    public void onStart() {
        speakMeaning(" ");
    }

    public void onPause() {
        stopSpeaker();
    }

    public EditorialBottomSheetBehaviour initUi(View rootView) {
        layoutBottomSheet = rootView.findViewById(R.id.bottom_sheet);
        bottomSheetHeader = rootView.findViewById(R.id.bottom_sheet_header);
        llSwipeUpMessage = rootView.findViewById(R.id.ll_swipe_up_message);
        layoutBottomSheetBorder = rootView.findViewById(R.id.bottom_sheet_border);
        tvTranslatedWord = rootView.findViewById(R.id.tv_translated_word);
        ivBack = rootView.findViewById(R.id.iv_back);

        WebView webView = rootView.findViewById(R.id.et_webView);
        webViewSupport = new WebViewSupport(webView);
        btnSpeaker = rootView.findViewById(R.id.iv_more_speaker);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        Button btnSaveWord = rootView.findViewById(R.id.btn_save_word);
        pbMeaning = rootView.findViewById(R.id.pb_meaning);

        bottomSheetHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mWord == null) {
                    showSliderErrorMessage();
                    return;
                }
                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });

        sheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (mWord == null) {
                    if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                    showSliderErrorMessage();
                    ivBack.setVisibility(View.GONE);
                }else {
                    ivBack.setVisibility(newState == BottomSheetBehavior.STATE_COLLAPSED ? View.GONE : View.VISIBLE);
                }
                if(newState == BottomSheetBehavior.STATE_COLLAPSED){
                    restoreBottomBarFields();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        btnSaveWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addWordInDatabase();
            }
        });

        updateSpeakerIcon();
        btnSpeaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ETPreferences.setSpeakerEnabled(activity,!ETPreferences.isSpeakerEnabled(activity));
                updateSpeakerIcon();
            }
        });
        return this;
    }

    private void updateSpeakerIcon() {
        btnSpeaker.setImageResource(ETPreferences.isSpeakerEnabled(activity)? R.drawable.ic_word_speaker : R.drawable.ic_speaker_disable);
    }

    public EditorialCallback.WordListener getWordListener() {
        return wordListener;
    }

    private final EditorialCallback.WordListener wordListener = new EditorialCallback.WordListener() {

        @Override
        public void onProgressUpdate(int visibility) {
            pbMeaning.setVisibility(visibility);
        }

        @Override
        public void onWordClicked(String word) {
            if(!TextUtils.isEmpty(word)) {
                showBottomBarFields();
                tvTranslatedWord.setText(word + " = ");
                ETNetworkUtil.getTranslateWord(activity, word, wordListener);
            }
        }

        @Override
        public void onTranslate(String word, String meaning) {
            mWord = word;
            mMeaning = meaning;
            //update slider view
            boolean isHindi = EditorialUtil.isWordInHindiLanguage(mWord);
            loadDictionary(mWord, isHindi);
            speakMeaning(meaning);
            tvTranslatedWord.setText(word + " = " + meaning);
            sheetBehavior.setDraggable(true);// If not found use version 1.2 or above of material design
            restoreBottomBarTimeHandler();
        }

        @Override
        public void onError(Exception e) {

        }
    };



    private void showBottomBarFields() {
        llSwipeUpMessage.setVisibility(View.VISIBLE);
        btnSpeaker.setVisibility(View.VISIBLE);
        layoutBottomSheetBorder.setVisibility(View.VISIBLE);
        bottomSheetHeader.setGravity(Gravity.CENTER);
        bottomSheetHeader.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimary));
    }

    private Handler handler = new Handler();

    private void restoreBottomBarTimeHandler() {
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    restoreBottomBarFields();
                }
            }, RESTORE_BOTTOM_BAR_AFTER_TIME_TAKEN);
        }
    }

    private void restoreBottomBarFields() {
        if (layoutBottomSheet != null) {
            sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
            if (sheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                bottomSheetHeader.post(new Runnable() {
                    @Override
                    public void run() {
                        tvTranslatedWord.setText(R.string.tap_on_word_for_meaning);
                        llSwipeUpMessage.setVisibility(View.GONE);
                        btnSpeaker.setVisibility(View.GONE);
                        ivBack.setVisibility(View.GONE);
                        layoutBottomSheetBorder.setVisibility(View.VISIBLE);
                        bottomSheetHeader.setGravity(Gravity.BOTTOM);
                        bottomSheetHeader.setBackgroundColor(Color.TRANSPARENT);
                        mWord = null;
                        mMeaning = null;
                        sheetBehavior.setDraggable(false);
                    }
                });
            }
        }

    }



    private void addWordInDatabase() {
        if (mWord != null && mMeaning != null) {
            if (dbHelper == null) {
                dbHelper = EditorialSdk.getInstance().getDBObject(activity);
            }
            TaskRunner.getInstance().executeAsync(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.callDBFunction(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            dbHelper.insertWord(mWord, mMeaning);
                            return null;
                        }
                    });
                    return null;
                }
            }, new TaskRunner.Callback<Void>() {
                @Override
                public void onComplete(Void result) {
                    Toast.makeText(activity, mWord + " added in vocabulary.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private boolean isMessageVisible = false;

    private void showSliderErrorMessage() {
        if (!isMessageVisible) {
            EditorialUtil.showToastCentre(activity, "Please select any word.");
            isMessageVisible = true;
        }
    }

    private void loadDictionary(String word, boolean isHindi) {
        String url = EditorialUtil.getDictWebUrl(word , isHindi );
        webViewSupport.getWebView().setBackgroundColor(ContextCompat.getColor(activity, R.color.themeBackgroundCardColor));
        webViewSupport.clearView();
        webViewSupport.loadUrl(url);
    }

    private void speakMeaning(String meaning) {
        if(ETPreferences.isSpeakerEnabled(activity)) {
            EditorialSdk.getInstance().getTextSpeech(activity).speakText(meaning);
        }
    }

    private void stopSpeaker() {
        EditorialSdk.getInstance().getTextSpeech(activity).stop();
    }

    public boolean isClosed() {
        if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            return false;
        }else {
            return true;
        }
    }
}
