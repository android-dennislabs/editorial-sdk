package com.editorial.listeners;

public enum EditorialMode {
    NORMAL, TAKE_NOTE
}
