package com.editorial.listeners;

import android.util.SparseArray;

import com.editorial.model.ETCategoryBean;
import com.editorial.model.ETServerBean;

import java.util.ArrayList;
import java.util.HashMap;

public interface EditorialCallback {

    interface Response<T> {
        void onSuccess(T response);

        void onFailure(Exception e);
    }

    interface OnCustomResponse {
        void onCustomResponse(boolean result);
    }

    interface NetworkListListener<T> {
        void onSuccess(ArrayList<T> response);

        void onDataRefresh(boolean refreshing);

        void onFailure(Exception e);
    }

    interface CategoryCallback {
        void onCategoryLoaded(ArrayList<ETCategoryBean> categoryBeen, SparseArray<String> categoryNames, SparseArray<String> categoryImages);
        void onFailure(Exception e);
    }


    interface OnArticleResponse {
        void onCustomResponse(boolean result, ETServerBean serverBean);
    }

    interface FontListener {
        void onIncreaseFontSize();
        void onDecreaseFontSize();
    }

    interface DayNightMode {
        void onSwitchDayNightMode(boolean isDayMode);
    }

    interface ShareListener {
        void onShareButtonClicked(int catId, int clickedId, String description);
    }

    interface WordListener {
        void onProgressUpdate(int visibility);

        void onWordClicked(String word);

        void onTranslate(String word, String meaning);

        void onError(Exception e);
    }

    interface SelectionCallback {
        void onAddPointClick(CharSequence word);
        void enableEditorMode();
    }
}
