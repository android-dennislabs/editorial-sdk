package com.editorial.util;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatDelegate;

import com.helper.util.DayNightPreference;

/**
 * Created by Amit on 1/7/2017.
 */

public class ETPreferences {

    public static final String FONT_SIZE = "font_size";
    public static final String KEY_TRANS = "trans";
    public static final String ZOOM_SIZE = "zoom_size";
    public static final String IS_ZOOM = "is_zoom_size";
    private static final String LOCATENAME = "locate_name";
    private static final String SPEAKER_MODE = "speaker_mode";

    private static SharedPreferences sharedPreferences;

    public static SharedPreferences getSharedPreferenceObj(Context context){
        if ( sharedPreferences == null )
            sharedPreferences = context.getSharedPreferences(context.getPackageName() , Context.MODE_PRIVATE);

        return sharedPreferences ;
    }

    public static boolean isDayMode(Context context){
        return !DayNightPreference.isNightModeEnabled(context);
    }

    public static void setDayMode(Context context , boolean isDayMode){
        DayNightPreference.setNightMode(context, !isDayMode);
    }

    public static String getKeyTrans(Context context){
        return getSharedPreferenceObj(context).getString( KEY_TRANS, "trnsl.1.1.20170110T180216Z.c772c6647fe329ac.6ce5ab0ea75909690a90860770565258c66ebb2e");
    }

    public static void setKeyTrans(Context context , String reg){
        getSharedPreferenceObj(context).edit().putString( KEY_TRANS, reg).apply();
    }

    public static void setSpeakerEnabled(Context context , boolean isEnable){
        getSharedPreferenceObj(context).edit().putBoolean( SPEAKER_MODE, isEnable).apply();
    }

    public static boolean isSpeakerEnabled(Context context ) {
        return getSharedPreferenceObj(context).getBoolean( SPEAKER_MODE , true);
    }

    public static int getFontSize(Context context){
        return getSharedPreferenceObj(context).getInt( FONT_SIZE , 17);
    }

    public static void setFontSize(Context context , int index){
        getSharedPreferenceObj(context).edit().putInt( FONT_SIZE, index).apply();
    }

    public static void setLocate(Context context,String locateName){
        getSharedPreferenceObj(context).edit().putString(LOCATENAME, locateName).apply();
    }

    public static String getLocateName(Context context){
        String defaultLocateName=context.getResources().getConfiguration().locale.toString();
        return getSharedPreferenceObj(context).getString(LOCATENAME,defaultLocateName);
    }


//    public static int getZoomSize(Context context){
//        return getSharedPreferenceObj(context).getInt( ZOOM_SIZE , 220);
//    }
//
//    public static void setZoomSize(Context context , int index){
//        getSharedPreferenceObj(context).edit().putBoolean( IS_ZOOM, true).commit();
//        getSharedPreferenceObj(context).edit().putInt( ZOOM_SIZE, index).commit();
//    }

    public static boolean getBoolean(Context context , String key){
        return getSharedPreferenceObj(context).getBoolean( key, false );
    }

    public static void setBoolean(Context context , String key , boolean value){
        getSharedPreferenceObj(context).edit().putBoolean( key, value ).apply();
    }

   public static String getString(Context context , String key){
        return getSharedPreferenceObj(context).getString( key, "" );
    }

    public static void setString(Context context , String key , String value){
        getSharedPreferenceObj(context).edit().putString( key, value ).apply();
    }

}
