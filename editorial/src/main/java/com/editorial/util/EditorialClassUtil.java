package com.editorial.util;

import android.content.Context;
import android.content.Intent;

import com.config.config.ConfigConstant;
import com.editorial.activity.ETCategoryActivity;
import com.editorial.activity.EditorialMultipleActivity;
import com.editorial.activity.PointsActivity;
import com.editorial.activity.TranslatorActivity;
import com.editorial.activity.WordsActivity;
import com.editorial.model.ETCategoryProperty;
import com.editorial.model.ETEditorialBean;
import com.editorial.model.ETEditorialProperty;
import com.editorial.util.database.ETDBConstant;

public class EditorialClassUtil {

//    public static void openEditorialActivity(Context context, int articleId, int catID, boolean isNotification , String host) {
//        openEditorialActivity(context, new ETEditorialBean().setId(articleId), catID, ETDBConstant.COLUMN_ID + "=" + articleId , isNotification);
//    }
//    public static void openEditorialActivity(Context context, int articleId, int catID, boolean isNotification) {
//        openEditorialActivity(context, new ETEditorialBean().setId(articleId), catID, ETDBConstant.COLUMN_ID + "=" + articleId , isNotification );
//    }
//    public static void openEditorialActivity(Context context, int articleId, int catID, String query) {
//        openEditorialActivity(context, new ETEditorialBean().setId(articleId), catID, query, false );
//    }
//
//    public static void openEditorialActivity(Context context, ETEditorialBean bean, int catID, String query ) {
//        openEditorialActivity(context, bean, catID, query , false);
//    }
    public static void openEditorialActivity(Context context, int articleId, int catID, boolean isNotification , String host) {
        openEditorialActivity(context, new ETEditorialBean().setId(articleId), catID, ETDBConstant.COLUMN_ID + "=" + articleId , isNotification , host);
    }
    public static void openEditorialActivity(Context context, int articleId, int catID, String query , String host) {
        openEditorialActivity(context, new ETEditorialBean().setId(articleId), catID, query, false , host);
    }

    public static void openEditorialActivity(Context context, ETEditorialBean bean, int catID, String query , String host ) {
        openEditorialActivity(context, bean, catID, query , false , host);
    }

    public static void openEditorialActivity(Context context, ETEditorialBean bean, int catID, String query , boolean isNotification , String host) {
        ETCategoryProperty property = ETCategoryProperty.Builder()
                .setCatId(catID)
                .setQuery(query)
                .setHost(host)
                .setNotification(isNotification);
        ETEditorialProperty editorial = ETEditorialProperty.Builder()
                .setEditorial(bean)
                .setCategoryProperty(property);
        Intent intent = new Intent(context, EditorialClassUtil.getEditorialClass());
        intent.putExtra(ETConstant.EDITORIAL_PROPERTY, editorial);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void openPointsActivity(Context context) {
        Intent intent = new Intent(context, EditorialClassUtil.getPointsClass());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void openWordsActivity(Context context) {
        Intent intent = new Intent(context, EditorialClassUtil.getWordsClass());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void openTranslatorActivity(Context context, String word) {
        Intent intent = new Intent(context, EditorialClassUtil.getTranslatorClass());
        intent.putExtra(ETConstant.TRANS_WORD, word);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static ETCategoryProperty getCategoryProperty(ETCategoryProperty categoryProperty, int catID) {
        ETCategoryProperty catProperty = categoryProperty.getClone();
        catProperty.setCatId(catID);
        return catProperty;
    }

    public static Class<?> getCategoryClass() {
        return ETCategoryActivity.class;
    }

    public static Class<?> getEditorialClass() {
        return EditorialMultipleActivity.class;
    }

    public static Class<?> getPointsClass() {
        return PointsActivity.class;
    }
    public static Class<?> getWordsClass() {
        return WordsActivity.class;
    }
    public static Class<?> getTranslatorClass() {
        return TranslatorActivity.class;
    }
}
