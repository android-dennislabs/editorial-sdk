package com.editorial.util;

public interface ETConstant {

    String DOWNLOAD_DIRECTORY = "Editorial";
    String HOST_DOMAIN = "https://topcoaching.in/";
    String DEEP_LINK_URL = "https://www.topcoaching.in/gkcurrentaffairs";
    String DICTIONARY_URL = "https://translate.google.co.in/#view=home&op=translate&sl=auto&";

    int ADS_BANNER = 0;
    String CAT_PROPERTY = "cat_property";
    String EDITORIAL_PROPERTY = "editorial_property";
    String CLICK_ITEM_ARTICLE = "_Click_Article";
    String TRANS_WORD = "trans_word";

    int DATABASE_VERSION = 1;
    long MAX_VALUE = 999999999;
    String NO_DATA = "No Data";
    String NO_INTERNET = "No Internet";
    String ERROR_INTEGRATION = "Something went wrong, Please try later.";
    String CATEGORY = "Category";

    boolean IS_ADS_ENABLED = true;
    int NATIVE_ADS_COUNT_LARGE = 4;
    int NATIVE_ADS_LIST_POSITION = 2;

    String TRANS_TYPE = "en-hi";
    String TRANS_TYPE_ENG = "en-hi";
    String TRANS_TYPE_HIN = "hi-en";

    String YANDEX_TRANSLATE = "yandex";
    String HOST_WORD_TRANSLATE = "host_word_translate";
}
