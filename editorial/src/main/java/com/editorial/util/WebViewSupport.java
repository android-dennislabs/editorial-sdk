package com.editorial.util;

import android.annotation.TargetApi;
import android.os.Build;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewSupport {
    private final WebView webView;

    public WebViewSupport(WebView webView) {
        this.webView = webView;
        init();
    }

    public WebView getWebView() {
        return webView;
    }

    private void init() {

        webView.setInitialScale(0);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
//                progressBar.setProgress(progress);
//                if (progress == 100) {
//                    llprogressBar.setVisibility(View.GONE);
//                    progressBar.setVisibility(View.GONE);
//                } else {
//                    llprogressBar.setVisibility(View.VISIBLE);
//                    progressBar.setVisibility(View.VISIBLE);
//                }
            }
        });

        webView.setWebViewClient(new WebViewClient() {

            // If you will not use this method url links are opeen in new brower
            // not in webview
            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.getUrl().toString());
                return true;
            }

            // Show loader on url load
            public void onLoadResource(WebView view, String url) {
                setHideGoogleTranslatorHeaderJavaScript(view);
            }

            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
                if (listener != null) {
                    listener.onLoadingCompleted();
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (listener != null) {
                    listener.onLoadingCompleted();
                }
            }
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                if(webView!=null){
                    ETLogger.d(error.getDescription() + "\n" + error.getErrorCode());
                }
            }

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                if(webView!=null){
                    ETLogger.d(description + "\n" + errorCode);
                }
            }

        });
        // Javascript inabled on webview
        webView.getSettings().setJavaScriptEnabled(true);

        // Other webview options
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);


//        webView.getSettings().setJavaScriptEnabled( true );
//        webView.getSettings().setSupportZoom(true);
//        webView.getSettings().setBuiltInZoomControls(true);
//        webView.getSettings().setDisplayZoomControls(false);
//        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
//        webView.setScrollbarFadingEnabled(false);

    }
    private void setHideGoogleTranslatorHeaderJavaScript(WebView view ){
        try {
            if ( url.contains(ETConstant.DICTIONARY_URL) ){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    view.evaluateJavascript(hideGoogleTranslatorHeaderJavaScript, null);
                } else {
                    view.loadUrl("javascript:"
                            + "var FunctionOne = function () {"
                            + "  try{"+hideGoogleTranslatorHeaderJavaScript+"}catch(e){}"
                            + "};");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final String hideGoogleTranslatorHeaderJavaScript = "document.getElementsByTagName('header')[0].style.display = 'none'";

    public void clearView() {
        if (Build.VERSION.SDK_INT < 18) {
            webView.clearView();
        } else {
            webView.loadUrl("about:blank");
        }
    }

    private String url ;
    public void loadUrl(String url){
        this.url = url ;
        webView.loadUrl(url);
    }

    private LoadingCompleteListener listener;

    public interface LoadingCompleteListener{
        void onLoadingCompleted();
    }

    public void setLoadingCompleteListener(LoadingCompleteListener listener) {
        this.listener = listener;
    }
}
