package com.editorial.util;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.text.Html;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.editorial.EditorialSdk;
import com.editorial.R;
import com.editorial.listeners.EditorialCallback;
import com.editorial.listeners.EditorialMode;
import com.editorial.model.ETEditorialBean;
import com.editorial.tasks.TaskDeletePoint;
import com.editorial.tasks.TaskShareContent;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.text.BreakIterator;
import java.util.List;
import java.util.Locale;

import static android.content.Context.CLIPBOARD_SERVICE;

public class ETTextEditor {

    private final Context context;

    public ETTextEditor(Context context) {
        this.context = context;
    }

    public void setClickableString(EditorialMode editorialMode, TextView textView, String desc, boolean isDayMode, EditorialCallback.WordListener listener, EditorialCallback.SelectionCallback textSelectionCallback) {
        setClickableString(editorialMode, textView, desc, isDayMode, ETPreferences.getFontSize(textView.getContext()), listener, textSelectionCallback);
    }

    public void setClickableString(EditorialMode editorialMode, TextView textView, String desc, boolean isDayMode, int textSize, EditorialCallback.WordListener listener, final EditorialCallback.SelectionCallback textSelectionCallback) {
        if (EditorialUtil.isEmptyOrNull(desc)) {
            return;
        }
        try {
            try {
                textView.setText(Html.fromHtml(desc));
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
                textView.setTextColor(isDayMode ? Color.BLACK : Color.WHITE);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (editorialMode == EditorialMode.NORMAL) {
                textView.setMovementMethod(LinkMovementMethod.getInstance());
                textView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        textSelectionCallback.enableEditorMode();
                        return false;
                    }
                });
                Spannable spans = (Spannable) textView.getText();
                BreakIterator iterator = BreakIterator.getWordInstance(Locale.US);
                try {
                    String datatoHTMLid = Html.fromHtml(desc).toString();
                    iterator.setText(datatoHTMLid);
                    int start = iterator.first();
                    for (int end = iterator.next(); end != BreakIterator.DONE; start = end, end = iterator
                            .next()) {
                        String possibleWord = datatoHTMLid.substring(start, end);
                        if (Character.isLetterOrDigit(possibleWord.charAt(0))) {
                            ClickableSpan clickSpan = getClickableSpan(textView.getContext(), possibleWord, isDayMode, listener);
                            spans.setSpan(clickSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                setEditorialPointMode(textView, textSelectionCallback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setEditorialPointMode(TextView textView, EditorialCallback.SelectionCallback textSelectionCallback) {
        textView.setTextIsSelectable(true);
        TextActionModeCallback callback = new TextActionModeCallback(context, textView, textSelectionCallback);
        textView.setCustomSelectionActionModeCallback(callback);
    }

    private ClickableSpan getClickableSpan(final Context context, final String word, final boolean isDayMode, final EditorialCallback.WordListener listener) {
        return new ClickableSpan() {
            final String mWord;

            {
                mWord = word;
            }

            @Override
            public void onClick(@NonNull View widget) {
                listener.onWordClicked(mWord);
            }

            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(isDayMode ? Color.BLACK : Color.WHITE);
            }
        };
    }

    public static void copyToClipBoard(Context context, CharSequence selectedText) {
        if(selectedText==null){
            return;
        }
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(selectedText, selectedText);
        clipboard.setPrimaryClip(clip);
    }

    public static CharSequence getSelectedText(TextView textView) {
        if(textView==null){
            return null;
        }
        int min = 0;
        int max = textView.getText().length();
        if (textView.isFocused()) {
            final int selStart = textView.getSelectionStart();
            final int selEnd = textView.getSelectionEnd();

            min = Math.max(0, Math.min(selStart, selEnd));
            max = Math.max(0, Math.max(selStart, selEnd));
        }
        return textView.getText().subSequence(min, max);
    }

    public void share(final View view, final int catId, final int clickedId, final String description, final WindowManager windowManager) {
        if(EditorialSdk.getInstance().isShareListenerAvailable()){
            EditorialSdk.getInstance().dispatchShareUpdated(catId, clickedId, description);
        }else {
            TedPermission.with(context)
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            new TaskShareContent(view, catId, clickedId, description, windowManager).execute();
                        }

                        @Override
                        public void onPermissionDenied(List<String> deniedPermissions) {
                            Toast.makeText(context, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                    .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .check();
        }
    }

    public void openFontDialog(Activity activity) {
        final EditorialCallback.FontListener listener = (EditorialCallback.FontListener)activity;
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.et_dialog_font, null);
        dialogView.findViewById(R.id.dlg_minus_font).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onIncreaseFontSize();
            }
        });
        dialogView.findViewById(R.id.dlg_plus_font).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDecreaseFontSize();
            }
        });
        dialogBuilder.setView(dialogView);
        dialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }


    public void openDayNightModeDialog(final Activity activity) {
        final EditorialCallback.DayNightMode listener = (EditorialCallback.DayNightMode)activity;
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.et_dlg_day_night_mode, null);
        final CheckBox cbDay = (CheckBox) dialogView.findViewById(R.id.dlg_cb_day);
        final CheckBox cbNight = (CheckBox) dialogView.findViewById(R.id.dlg_cb_night);
        (ETPreferences.isDayMode(activity) ? cbDay : cbNight).setChecked(true);
        dialogView.findViewById(R.id.dlg_rl_day).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ETPreferences.isDayMode(activity)) {
                    listener.onSwitchDayNightMode(true);
                    cbDay.setChecked(true);
                    cbNight.setChecked(false);
                }
            }
        });
        dialogView.findViewById(R.id.dlg_rl_night).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ETPreferences.isDayMode(activity)) {
                    listener.onSwitchDayNightMode(false);
                    cbDay.setChecked(false);
                    cbNight.setChecked(true);
                }
            }
        });
        dialogBuilder.setView(dialogView);
        dialogBuilder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    public void deletePoint(final Activity activity, ETEditorialBean item) {
        new TaskDeletePoint(activity, 0, item, new EditorialCallback.Response<Integer>() {
            @Override
            public void onSuccess(Integer response) {
                EditorialUtil.showToastCentre(activity,"Point deleted successful.");
                activity.finish();
            }

            @Override
            public void onFailure(Exception e) {

            }
        }).execute();
    }
}
