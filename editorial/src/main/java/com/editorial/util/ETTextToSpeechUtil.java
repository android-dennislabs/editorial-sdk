package com.editorial.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.util.HashMap;
import java.util.Locale;

/**
 * Created by dennis-01 on 10/2/2015.
 */
public class ETTextToSpeechUtil {

    private static final float DEFAULT_SPEED = 1.0f;
    @SuppressLint("StaticFieldLeak")
    private static ETTextToSpeechUtil textToSpeechUtil;
    private static TextToSpeech textToSpeech;
    @SuppressLint("StaticFieldLeak")
    private static Context context;
    private static String testString = "";
    private final String utteranceId = "you_utterance_id";

    private ETTextToSpeechUtil() {
        initTextToSpeech(context);
    }

    public static void playTextToSpeech(String msg, final float speed) {
        if (textToSpeech.isSpeaking()) {
            textToSpeech.stop();
        }
        textToSpeech.setPitch(1.0f);
        textToSpeech.setSpeechRate(speed);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP)
            textToSpeech.speak(msg, TextToSpeech.QUEUE_FLUSH, null, null);
        else {
            textToSpeech.speak(msg, TextToSpeech.QUEUE_FLUSH, null);
        }

    }

    private void initTextToSpeech(final Context context) {
        textToSpeech = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    textToSpeech.setLanguage(new Locale(ETPreferences.getLocateName(context)));
                    playTextToSpeech(testString, 1.0f);
                }
            }
        });
    }

    public static ETTextToSpeechUtil getInstance(Context mContext) {
        testString = "";
        if (textToSpeechUtil == null) {
            context = mContext;
            textToSpeechUtil = new ETTextToSpeechUtil();
        }
        return textToSpeechUtil;
    }

    public static ETTextToSpeechUtil ForceFullyChangeLocate(Context mContext, String test) {
        context = mContext;
        testString = test;
        textToSpeechUtil = new ETTextToSpeechUtil();
        return textToSpeechUtil;
    }

    public void speakText(String data) {
        speakText(data, DEFAULT_SPEED);
    }

    public void stop() {
        if (textToSpeech != null) {
            textToSpeech.stop();
        }
    }

    public void speakText(String data, final float speed) {
        if (textToSpeech != null && data != null) {
            if (textToSpeech.isSpeaking()) {
                textToSpeech.stop();
            }
            int result = textToSpeech.setLanguage(EditorialUtil.isWordInHindiLanguage(data) ? new Locale("hin-IND") : Locale.ENGLISH);
            textToSpeech.setPitch(1.0f);
            textToSpeech.setSpeechRate(speed);
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                HashMap<String, String> params = new HashMap<>();
                params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, utteranceId);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    if(data.length() > TextToSpeech.getMaxSpeechInputLength()) {
                        data = data.substring(0, TextToSpeech.getMaxSpeechInputLength());
                    }
                }
                textToSpeech.speak(data, TextToSpeech.QUEUE_FLUSH, params);
            }
        }
    }
}
