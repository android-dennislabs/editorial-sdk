package com.editorial.util.database;

public interface ETDBConstant {

    String ID = "id";
    String COLUMN_DESC = "description";
    String COLUMN_MOCK_TEST_ID = "mock_test_id";
    String COLUMN_TOTAL_QUE = "total_que";
    String COLUMN_SKIP_QUE = "skip_que";
    String COLUMN_CORRECT_ANS = "correct_ans";
    String COLUMN_WRONG_ANS = "wrong_ans";
    String COLUMN_TIME_INIT = "time_init";
    String COLUMN_TIME_FINISH = "time_finish";
    String COLUMN_QUE = "que";
    String COLUMN_ANS = "ans";
    String COLUMN_OPT_A = "opt_a";
    String COLUMN_OPT_B = "opt_b";
    String COLUMN_OPT_C = "opt_c";
    String COLUMN_OPT_D = "opt_d";
    String COLUMN_OPT_E = "opt_e";
    String COLUMN_DIRECTION = "direction";
    String COLUMN_TITLE = "title";
    String COLUMN_DATA = "data";
    String COLUMN_ID = "id";
    String COLUMN_CAT_ID = "cat_id";
    int INT_TRUE = 1;
    int INT_FALSE = 0;
    String COLUMN_ATTEMPTED = "is_attempted";
    String COLUMN_DOWNLOADED = "is_downloaded";
    int QUE_NUM = 20;

    String COLUMN_INSTRUCTION = "instruction";
    String COLUMN_TEST_TIME = "test_time";
    String COLUMN_TEST_MARKS = "test_marks";
    String COLUMN_QUEST_MARKS = "quest_marks";
    String COLUMN_NEGATIVE_MARKING = "negative_marking";

    String COLUMN_SUB_CAT_ID = "sub_cat_id";
    String COLUMN_SUB_CAT_NAME = "sub_cat_name";
    String IMAGE = "image";

    String IS_SYNC = "is_sync";
     String COLUMN_FAV = "is_fav";

}
