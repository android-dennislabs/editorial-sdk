package com.editorial.util.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

import com.adssdk.AdsSDK;
import com.editorial.model.ETCatBean;
import com.editorial.model.ETCategoryBean;
import com.editorial.model.ETEditorialBean;
import com.editorial.model.ETHomeBean;
import com.editorial.model.ETIdBean;
import com.editorial.model.ETListBean;
import com.editorial.model.ETServerBean;
import com.editorial.model.ETVocubModel;
import com.editorial.util.DateTimeUtil;
import com.editorial.util.ETConstant;
import com.editorial.util.ETLogger;
import com.editorial.util.EditorialUtil;
import com.helper.util.BaseDatabaseHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


public class ETDbHelper extends BaseDatabaseHelper {

    private static final String DB_NAME = "editorial.sqlite";
    private static final int DATABASE_VERSION = ETConstant.DATABASE_VERSION;

    static SQLiteDatabase db;
    Context context;

    public String TABLE_EDITORIAL = "editorial_list";
    public String TABLE_POINTS = "point_list";
    public String TABLE_WORDS = "saved_words";
    private String TABLE_SUB_CAT = "category_list";
    private String COLUMN_DESC = "description";
    private String COLUMN_DATE = "date";
    public static String COLUMN_TITLE = "title";
    public static String COLUMN_ID = "id";
    public static String COLUMN_AUTO_ID = "auto_id";
    public static String COLUMN_CAT_ID = "cat_id";
    public static String COLUMN_CAT_NAME = "cat_name";
    public static String COLUMN_WORD = "word";
    public static String COLUMN_MEANING = "meaning";
    public static String COLUMN_TAG = "tag";
    public static String COLUMN_SUB_CAT_ID = "sub_cat_id";
    private String COLUMN_SUB_CAT_NAME = "sub_cat_name";
    public static String COLUMN_READ = "is_read";
    public static String COLUMN_FAV = "is_fav";
    public static final int INT_TRUE = 1;
    public static final int INT_FALSE = 0;

    private String IMAGE = "image";


    private String CREATE_TABLE_EDITORIAL_LIST = "CREATE TABLE IF NOT EXISTS editorial_list (" +
            "    id          INTEGER PRIMARY KEY," +
            "    title       VARCHAR," +
            "    description VARCHAR," +
            "    is_read     INTEGER DEFAULT (0)," +
            "    is_fav      INTEGER DEFAULT (0)," +
            "    sub_cat_id  INTEGER," +
            "    cat_id      INTEGER," +
            "    tag         VARCHAR," +
            "    date        VARCHAR" +
            ");";

    private String CREATE_TABLE_POINT_LIST = "CREATE TABLE IF NOT EXISTS point_list (" +
            "    auto_id     INTEGER PRIMARY KEY AUTOINCREMENT," +
            "    id          INTEGER," +
            "    title       VARCHAR," +
            "    description VARCHAR," +
            "    is_read     INTEGER DEFAULT (0)," +
            "    is_fav      INTEGER DEFAULT (0)," +
            "    sub_cat_id  INTEGER," +
            "    cat_id      INTEGER," +
            "    cat_name    VARCHAR," +
            "    tag         VARCHAR," +
            "    date        VARCHAR" +
            ");";

    private String CREATE_TABLE_SAVED_WORDS = "CREATE TABLE IF NOT EXISTS saved_words (" +
            "    id          INTEGER PRIMARY KEY AUTOINCREMENT," +
            "    word       VARCHAR," +
            "    meaning VARCHAR," +
            "    date        VARCHAR" +
            ");";

    private String CREATE_TABLE_CATEGORY_LIST = "CREATE TABLE IF NOT EXISTS category_list (" +
            "    cat_id       INTEGER," +
            "    sub_cat_id   INTEGER," +
            "    ranking      INTEGER DEFAULT (0)," +
            "    categoryType INTEGER DEFAULT (0)," +
            "    raw_data     VARCHAR ," +
            "    sub_cat_name VARCHAR ," +
            "    image        VARCHAR ," +
            "    order_id     INTEGER" +
            ");";


    private ETDbHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    private static ETDbHelper instance;

    public static ETDbHelper getInstance(Context context) {
        if (instance == null)
            instance = new ETDbHelper(context);
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_EDITORIAL_LIST);
        db.execSQL(CREATE_TABLE_SAVED_WORDS);
        db.execSQL(CREATE_TABLE_POINT_LIST);
        db.execSQL(CREATE_TABLE_CATEGORY_LIST);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        switch (oldVersion) {
            case 1:
//                db.execSQL(CREATE_TABLE_EDITORIAL_LIST);
//                db.execSQL(CREATE_TABLE_CATEGORY_LIST);
//                db.execSQL(CREATE_TABLE_BOOKS);
//                db.execSQL(CREATE_TABLE_SUBJECT);

        }
    }

    private SQLiteDatabase openDataBase(Context context) {
        SQLiteDatabase database = getWritableDatabase();
        if (database == null)
            instance = new ETDbHelper(context);
        database = getWritableDatabase();
        return database;
    }

    public void callDBFunction(Callable<Void> function) {
        try {
            db = getDB();
            db.beginTransaction();
            function.call();
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
            db = getDB();
            db.setTransactionSuccessful();
            db.endTransaction();
            ETLogger.e(e.getMessage());
        }
    }

    private SQLiteDatabase getDB() {
        SQLiteDatabase database = getWritableDatabase();
        return database;
    }

    public ArrayList<ETCategoryBean> fetchCategoryData(int catId, int[] images, int defaultImg) {
        ArrayList<ETCategoryBean> categoryBeen = null;
        Cursor cursor = db.query(TABLE_SUB_CAT, null, COLUMN_CAT_ID + "=" + catId, null, null, null, COLUMN_SUB_CAT_ID + " ASC");
        categoryBeen = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            ETCategoryBean bean;
            int count = 0;
            do {
                bean = new ETCategoryBean();
                bean.setCategoryId(cursor.getInt(cursor.getColumnIndex(COLUMN_SUB_CAT_ID)));
                bean.setCategoryName(cursor.getString(cursor.getColumnIndex(COLUMN_SUB_CAT_NAME)));
                if (images != null && images.length > count)
                    bean.setCategoryImage(images[count++]);
                else
                    bean.setCategoryImage(defaultImg);
                categoryBeen.add(bean);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return categoryBeen;
    }

    private Cursor getHomeArticleCursor(String query, int catId) {
        try {
            if (!EditorialUtil.isEmptyOrNull(query))
                query = " AND " + query;
            return db.query(getArticleTableName(catId), null, COLUMN_TITLE + "!='NULL'" + query, null, null, null, COLUMN_ID + " DESC");
        } catch (SQLException e) {
            return null;
        }
    }


    public String[] fetchLatestEmptyIds(String catQuery, int catId, Context context) {
        String[] ids = null;
        if (!EditorialUtil.isEmptyOrNull(catQuery))
            catQuery = catQuery + " AND ";
        Cursor cursor = db.query(getArticleTableName(catId), new String[]{COLUMN_ID}, catQuery + COLUMN_TITLE + " is null or " + COLUMN_TITLE + " = ''",
                null, null, null, COLUMN_ID + " DESC LIMIT 100 ");

        if (cursor != null && cursor.getCount() > 0) {
            ids = new String[cursor.getCount()];
            int i = 0;
            cursor.moveToFirst();
            do {
                ids[i++] = cursor.getInt(cursor.getColumnIndex(COLUMN_ID)) + "";
            } while (cursor.moveToNext());
        }
        cursor.close();
        return ids;
    }

    public String convertToDateFormat(String args) {
        if (args != null && !TextUtils.isEmpty(args)) {
            args = args.split("\\s+")[0];
            if (args.contains(" "))
                args = args.split(" ")[0];
            String[] arr = args.split("-");
            StringBuilder stringBuilder = new StringBuilder(arr[2]);
            stringBuilder.append(" ").append(MONTHS_NAME[Integer.parseInt(arr[1]) - 1]).append(" ").append(arr[0]);
            return stringBuilder.toString();
        }

        return args;
    }

    public void insertEditorial(List<ETServerBean> idBeen, int catId, boolean isDelete) {
        if (isDelete) {
            try {
                String query = COLUMN_CAT_ID + "=" + catId + " AND " + COLUMN_FAV + "=" + INT_FALSE
                        + " AND " + COLUMN_READ + "=" + INT_FALSE;
                int i = db.delete(getArticleTableName(catId), query, null);
                ETLogger.e("insertEditorial delete : " + i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        String tableName = getArticleTableName(catId);
        for (ETServerBean homeBean : idBeen) {
            Cursor cursor = db.query(tableName, new String[]{COLUMN_ID}, COLUMN_TITLE + "!='NULL' AND " + COLUMN_ID + "='" + homeBean.getId() + "'",
                    null, null, null, null);
            if (!(cursor != null && cursor.getCount() > 0)) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(COLUMN_ID, homeBean.getId());
                contentValues.put(COLUMN_TITLE, homeBean.getTitle());
                contentValues.put(COLUMN_DATE, convertToDateFormat(homeBean.getUpdatedAt()));
                contentValues.put(COLUMN_DESC, homeBean.getDescription());
                contentValues.put(COLUMN_TAG, homeBean.getOptionA());

                int c = db.update(tableName, contentValues, COLUMN_ID + "=" + homeBean.getId(), null);
                ETLogger.e("insertEditorial update : " + c);
                if (c == 0) {
                    contentValues.put(COLUMN_CAT_ID, catId);
                    contentValues.put(COLUMN_SUB_CAT_ID, homeBean.getCategoryId());
                    try {
                        long l = db.insertOrThrow(tableName, null, contentValues);
                        ETLogger.e("insertEditorial insertOrThrow : " + l);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
            cursor.close();
        }
    }

    public void insertPoint(ETEditorialBean mItem, ETEditorialBean mEditorial, CharSequence word, int catId) {

        Cursor cursor = db.query(TABLE_POINTS, new String[]{COLUMN_AUTO_ID}, COLUMN_DESC + "='" +sqlEscapeString(word.toString()) + "' AND " + COLUMN_ID + "='" + mEditorial.getId() + "'",
                null, null, null, null);

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_ID, mEditorial.getId());
        contentValues.put(COLUMN_TITLE, sqlEscapeString(mEditorial.getTitle()));
        contentValues.put(COLUMN_DATE, DateTimeUtil.getDateTime());
        contentValues.put(COLUMN_DESC, sqlEscapeString(word.toString()));
        contentValues.put(COLUMN_TAG, mItem.getTag());
        contentValues.put(COLUMN_CAT_NAME, mItem.getCategory());

        if (cursor != null && cursor.moveToFirst()) {
            int columnId = cursor.getInt(cursor.getColumnIndex(COLUMN_AUTO_ID));
            db.update(TABLE_POINTS, contentValues, COLUMN_AUTO_ID + "=" + columnId, null);
        } else {
            ETLogger.e("insertEditorial update : ");
            contentValues.put(COLUMN_CAT_ID, catId);
            contentValues.put(COLUMN_SUB_CAT_ID, mEditorial.getSubCatId());
            try {
                long l = db.insertOrThrow(TABLE_POINTS, null, contentValues);
                ETLogger.e("insertEditorial insertOrThrow : " + l);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (cursor != null) {
            cursor.close();
        }
    }

    public void insertWord(CharSequence word, CharSequence meaning) {

        Cursor cursor = db.query(TABLE_WORDS, new String[]{COLUMN_ID}, COLUMN_WORD + "='" + sqlEscapeString(word.toString()) + "'",
                null, null, null, null);

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_WORD, sqlEscapeString(word.toString()));
        contentValues.put(COLUMN_MEANING, sqlEscapeString(meaning.toString()));
        contentValues.put(COLUMN_DATE, DateTimeUtil.getDateTime());

        if (cursor != null && cursor.moveToFirst()) {
            int columnId = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
            db.update(TABLE_WORDS, contentValues, COLUMN_ID + "=" + columnId, null);
        } else {
            ETLogger.e("insertEditorial update : ");
            try {
                long l = db.insertOrThrow(TABLE_WORDS, null, contentValues);
                ETLogger.e("insertEditorial insertOrThrow : " + l);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (cursor != null) {
            cursor.close();
        }
    }

    public void insertCat(List<ETCatBean> catBeen) {
        for (ETCatBean bean : catBeen) {
            insertCatData(bean);
        }
    }

    private void insertCatData(ETCatBean bean) {
        String where = COLUMN_CAT_ID + "=" + bean.getParent_id() + " AND " + COLUMN_SUB_CAT_ID + "=" + bean.getId();
        Cursor cursor = db.query(TABLE_SUB_CAT, null, where, null, null, null, null);
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_CAT_ID, bean.getParent_id());
        contentValues.put(COLUMN_SUB_CAT_ID, bean.getId());
        contentValues.put(COLUMN_SUB_CAT_NAME, bean.getTitle());
        contentValues.put(IMAGE, bean.getImage());
        if (!(cursor != null && cursor.getCount() > 0 && cursor.moveToFirst())) {
            cursor.close();
            try {
                db.insertOrThrow(TABLE_SUB_CAT, null, contentValues);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                db.update(TABLE_SUB_CAT, contentValues, where, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private boolean isNativeAd(int position) {
        return position == 2 || position == 12;
    }


    public void fetchHomeData(ArrayList<ETEditorialBean> list, String query, int catId, boolean isNotFavTab) {
        ETEditorialBean bean;
        ArrayList<ETEditorialBean> been = new ArrayList<>();
        Cursor cursor = getHomeArticleCursor(query, catId);
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            int countAds = 0;
            int lastId = 0;
            do {
//    				if ( countAds % ETConstant.NATIVE_ADS_COUNT_MEDIUM == 0 ) {
                if (isNativeAd(countAds) && ETConstant.IS_ADS_ENABLED) {
                    bean = new ETEditorialBean();
                    bean.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                    bean.setId(lastId);
                    been.add(bean);
//    					countAds = 0 ;
                    countAds++;
                }
                countAds++;
                bean = new ETEditorialBean();
                bean.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                lastId = bean.getId();
                bean.setTrue(cursor.getInt(cursor.getColumnIndex(COLUMN_READ)) != 0);
                bean.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                bean.setDesc(cursor.getString(cursor.getColumnIndex(COLUMN_DESC)));
                bean.setTag(cursor.getString(cursor.getColumnIndex(COLUMN_TAG)));
                bean.setDate(cursor.getString(cursor.getColumnIndex(COLUMN_DATE)));
                bean.setCatId(cursor.getInt(cursor.getColumnIndex(COLUMN_CAT_ID)));
                bean.setSubCatId(cursor.getInt(cursor.getColumnIndex(COLUMN_SUB_CAT_ID)));
                bean.setFav(cursor.getInt(cursor.getColumnIndex(COLUMN_FAV)) == INT_TRUE);
                been.add(bean);
            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
        if (!isNotFavTab || been.size() > 0) {
            list.clear();
            list.addAll(been);
        }
    }


    public void updateArticle(int id, String columnName, int value, int catId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(columnName, value);
        db.update(getArticleTableName(catId), contentValues, COLUMN_ID + "=" + id, null);
    }


    public void insertArticleId(List<ETIdBean> idBeen, int catID) {
        for (ETIdBean bean : idBeen) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_ID, bean.getId());
            contentValues.put(COLUMN_SUB_CAT_ID, bean.getCategoryId());
            contentValues.put(COLUMN_CAT_ID, catID);
            Cursor cursor = db.query(getArticleTableName(catID), null, COLUMN_ID + "=" + bean.getId(), null, null, null, null);
            if (cursor == null || cursor.getCount() < 1)
                try {
                    db.insertOrThrow(getArticleTableName(catID), null, contentValues);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            cursor.close();
        }
    }


    public ArrayList<ETCategoryBean> MockFetchCategoryData(String query, int[] images, int defaultImg) {
        ArrayList<ETCategoryBean> list = new ArrayList<>();
        Cursor cursor = db.query(TABLE_SUB_CAT, null, query, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            list.clear();
            ETCategoryBean bean;
            int count = 0;
            do {
                bean = new ETCategoryBean();
                bean.setCategoryName(cursor.getString(cursor.getColumnIndex(COLUMN_SUB_CAT_NAME)));
                bean.setCategoryId(cursor.getInt(cursor.getColumnIndex(COLUMN_SUB_CAT_ID)));
                String imageUrl = cursor.getString(cursor.getColumnIndex(IMAGE));
                if (!EditorialUtil.isEmptyOrNull(imageUrl))
                    bean.setImageUrl(imageUrl);
                else if (images != null && images.length > count)
                    bean.setCategoryImage(images[count++]);
                else
                    bean.setCategoryImage(defaultImg);
                list.add(bean);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }


    private String getArticleTableName(int catID) {
        return TABLE_EDITORIAL;
    }


    public String[] MONTHS_NAME = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct"
            , "Nov", "Dec"};


    public ETEditorialBean fetchDescData(String query, int catId) {
        ETEditorialBean bean = null;
        Cursor cursor = getHomeArticleCursor(query, catId);
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            bean = new ETEditorialBean();
            bean.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
            bean.setFav(cursor.getInt(cursor.getColumnIndex(COLUMN_FAV)) != 0);
            bean.setRead(cursor.getInt(cursor.getColumnIndex(COLUMN_READ)) != 0);
            bean.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
            bean.setDate(cursor.getString(cursor.getColumnIndex(COLUMN_DATE)));
            bean.setDesc(removeBottomPadding2(cursor.getString(cursor.getColumnIndex(COLUMN_DESC))));
        }
        if (cursor != null) {
            cursor.close();
        }

        return bean;
    }


    public int fetchDescData(ArrayList<ETEditorialBean> list, String query, int id, int catId) {
        ETEditorialBean bean;
        int position = 0;
        ArrayList<ETEditorialBean> been = new ArrayList<>();
        Cursor cursor = getHomeArticleCursor(query, catId);
        if (cursor != null && cursor.getCount() > 0) {
            int count = 0;
            cursor.moveToFirst();
            int countAds = 0;
            do {
                if (countAds == ETConstant.NATIVE_ADS_COUNT_LARGE && ETConstant.IS_ADS_ENABLED) {
                    bean = new ETEditorialBean();
                    bean.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                    been.add(bean);
                    countAds = 0;
                    count++;
                } else
                    countAds++;
                bean = new ETEditorialBean();
                bean.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                bean.setFav(cursor.getInt(cursor.getColumnIndex(COLUMN_FAV)) != 0);
                bean.setRead(cursor.getInt(cursor.getColumnIndex(COLUMN_READ)) != 0);
                bean.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                bean.setDate(cursor.getString(cursor.getColumnIndex(COLUMN_DATE)));
                bean.setCatId(cursor.getInt(cursor.getColumnIndex(COLUMN_CAT_ID)));
                bean.setSubCatId(cursor.getInt(cursor.getColumnIndex(COLUMN_SUB_CAT_ID)));
                bean.setDesc(removeBottomPadding2(cursor.getString(cursor.getColumnIndex(COLUMN_DESC))));
                bean.setTag(removeBottomPadding2(cursor.getString(cursor.getColumnIndex(COLUMN_TAG))));
                been.add(bean);
                if (bean.getId() == id)
                    position = count;
                count++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        if (been.size() > 0) {
            list.clear();
            list.addAll(been);
        }
        return position;
    }


    public void fetchPointsData(ArrayList<ETEditorialBean> list) {
        ETEditorialBean bean;
        ArrayList<ETEditorialBean> been = new ArrayList<>();
        Cursor cursor = db.query(TABLE_POINTS, null, COLUMN_TITLE + "!='NULL'", null, null, null, COLUMN_AUTO_ID + " DESC");
        if (cursor != null && cursor.getCount() > 0) {
            int count = 0;
            cursor.moveToFirst();
            int countAds = 0;
            do {
                if (countAds == ETConstant.NATIVE_ADS_LIST_POSITION && ETConstant.IS_ADS_ENABLED) {
                    bean = new ETEditorialBean();
                    bean.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                    been.add(bean);
                    countAds++;
                    count++;
                } else
                    countAds++;
                bean = new ETEditorialBean();
                bean.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_AUTO_ID)));
                bean.setFav(cursor.getInt(cursor.getColumnIndex(COLUMN_FAV)) != 0);
                bean.setRead(cursor.getInt(cursor.getColumnIndex(COLUMN_READ)) != 0);
                bean.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                bean.setTag(cursor.getString(cursor.getColumnIndex(COLUMN_TAG)));
                bean.setDate(DateTimeUtil.convertServerDateTime(cursor.getString(cursor.getColumnIndex(COLUMN_DATE))));
                bean.setCategory(cursor.getString(cursor.getColumnIndex(COLUMN_CAT_NAME)));
                bean.setDesc(removeBottomPadding2(cursor.getString(cursor.getColumnIndex(COLUMN_DESC))));
                been.add(bean);
                count++;
            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
        if (been.size() > 0) {
            list.clear();
            list.addAll(been);
        }
    }

    private String removeBottomPadding2(String s) {
        if ( !TextUtils.isEmpty( s ) ) {
            if (s.contains("<p>&nbsp;</p>"))
                s = s.replaceAll("<p>&nbsp;</p>", "");
        }
        return s;
    }

    public void fetchVocubData(ArrayList<ETVocubModel> list) {
        ETVocubModel bean;
        ArrayList<ETVocubModel> been = new ArrayList<>();
        Cursor cursor = db.query(TABLE_WORDS, null, COLUMN_WORD + "!='NULL'", null, null, null, COLUMN_ID + " DESC");
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            int countAds = 0;
            do {
                if (countAds == ETConstant.NATIVE_ADS_LIST_POSITION && ETConstant.IS_ADS_ENABLED) {
                    bean = new ETVocubModel();
                    bean.setModelId(AdsSDK.NATIVE_ADS_MODEL_ID);
                    been.add(bean);
                    countAds ++;
                } else
                    countAds++;
                bean = new ETVocubModel();
                bean.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                bean.setWord(cursor.getString(cursor.getColumnIndex(COLUMN_WORD)));
                bean.setMeaning(cursor.getString(cursor.getColumnIndex(COLUMN_MEANING)));
                bean.setDate(DateTimeUtil.convertServerDateTime(cursor.getString(cursor.getColumnIndex(COLUMN_DATE))));
                been.add(bean);
            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
        if (been.size() > 0) {
            list.clear();
            list.addAll(been);
        }
    }

    public void deletePoint(ETEditorialBean item) {
        try {
            String query = COLUMN_AUTO_ID + "=" + item.getId();
            int i = db.delete(TABLE_POINTS, query, null);
            ETLogger.e("deletePoint : " + i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteWord(ETVocubModel item) {
        try {
            String query = COLUMN_ID + "=" + item.getId();
            int i = db.delete(TABLE_WORDS, query, null);
            ETLogger.e("deleteWord : " + i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
