package com.editorial.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.adssdk.AdsSDK;
import com.adssdk.NativeUnifiedAdsFullViewHolder;
import com.adssdk.NativeUnifiedAdsViewHolder;
import com.adssdk.nativead.NativeAdsUIUtil;
import com.editorial.EditorialSdk;
import com.editorial.R;

public class EditorialUtil {

    public static void initAds(RelativeLayout view, Activity context, int type) {
        if (EditorialSdk.getInstance().getAdsSDK() != null) {
            EditorialSdk.getInstance().getAdsSDK().setAdoptiveBannerAdsOnView(view , context);
        }
    }

    public static void initAds(RelativeLayout view , Activity activity) {
        if (EditorialSdk.getInstance().getAdsSDK() != null) {
            EditorialSdk.getInstance().getAdsSDK().setAdoptiveBannerAdsOnView(view , activity);
        }
    }

    private static NativeUnifiedAdsViewHolder getNative(RelativeLayout rlNativeAd, int layoutRes) {
        View view = LayoutInflater.from(rlNativeAd.getContext()).inflate(layoutRes, null, false);
        rlNativeAd.removeAllViews();
        rlNativeAd.addView(view);
        return new NativeUnifiedAdsFullViewHolder(rlNativeAd);
    }

    public static void loadNativeAd(final Activity activity ,final RelativeLayout relativeLayout, final boolean isSmall, final int layoutRes) {
        if (AdsSDK.getInstance() != null ) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (relativeLayout != null) {
                        NativeAdsUIUtil.bindUnifiedNativeAd(activity , getNative(relativeLayout, layoutRes), isSmall);
                    }
                }
            }, 100);
        }
    }

    public static boolean isWordInHindiLanguage(String text) {
        boolean isHindi  = false;
        for (char c: text.toCharArray()) {
            if (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.DEVANAGARI) {
                isHindi = true;
                break;
            }
        }
        return isHindi;
    }
    public static boolean isEmptyOrNull(String s) {
        return (s == null || TextUtils.isEmpty(s));
    }

    public static String getMockImageUrl() {
        return ETConstant.HOST_DOMAIN + "uploads/crudfiles/";
    }

    public static void loadUserImage(String photoUrl, String baseUrl, ImageView ivProfilePic, int res) {
        if (!isEmptyOrNull(photoUrl)) {
            if (photoUrl.startsWith("http://") || photoUrl.startsWith("https://")) {
            } else
                photoUrl = baseUrl + photoUrl;
            EditorialSdk.getInstance().getPicasso().load(photoUrl)
                    .resize(80, 80)
                    .centerCrop()
                    .placeholder(res)
                    .transform(new CircleTransform())
                    .into(ivProfilePic);

        } else
            ivProfilePic.setImageResource(res);
    }

    public static void shareArticle(Context context, String message, int catId, int id) {
//        String strUri = getArticleUri(catId, id);
        String strUri = getAppDownloadLink(context);
        Intent in = new Intent("android.intent.action.SEND");
        in.setType("text/plain");
        in.putExtra("android.intent.extra.TEXT", strUri);
//        in.putExtra("android.intent.extra.TEXT", message + "\nOpen Article: " + strUri);
        context.startActivity(Intent.createChooser(in, "Share With Fiends"));
    }

    public static void share(Context context, Uri uri, String deepLink) {
//        String text = "\nChick here to open : \n" + deepLink;
        String text = deepLink;

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.setType("image/*");
//        intent.setPackage("com.whatsapp");
        context.startActivity(Intent.createChooser(intent, "Share With"));
//        context.startActivity(intent);
    }
    public static String getAppDownloadLink(Context context) {
        return "Download " + context.getString(R.string.app_name)
                + " app. \nLink : http://play.google.com/store/apps/details?id=" + context.getPackageName();
    }
    public static String getArticleUri(int catId, int id) {
        int type;
        boolean isWebView = false;
        if (isWebView) {
            type = 0;
        } else {
            type = 1;
        }
        return EditorialSdk.getInstance().getDeepLinkUrl() + "?article=" + catId + "," + id + "," + type;
    }

    public static void showToastInternet(Context context) {
        Toast toast = Toast.makeText(context, "No internet connection. ", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static void showToastCentre(Context context, String msg) {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }


    public static boolean isConnected(Context context) {
        if (context != null) {
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            return activeNetwork != null &&
                    activeNetwork.isConnected();
        }
        return false;
    }
    public static void hideView(View view) {
        if (view != null) {
            view.setVisibility(View.GONE);
        }
    }

    public static void showView(View view) {
        if (view != null) {
            view.setVisibility(View.VISIBLE);
        }
    }

    public static void showDialog(Context context, String title, String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
//                 .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    public static String getDictWebUrl(String word, boolean isHindi) {
        return ETConstant.DICTIONARY_URL + "tl="+ ( isHindi ? "en" : "hi") +"&text=" + word ;
    }

}
