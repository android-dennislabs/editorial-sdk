package com.editorial.util;

import android.text.format.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTimeUtil {

    public static String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getTimeStamp() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyyMMdd-HHmmss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String convertServerDateTime(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yy hh:mm:a", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = convertTimeStamp(date.getTime()).toString();
        } catch (ParseException | NullPointerException e) {
            outputDateStr="Not available";
        }
        return outputDateStr;
    }

    public static String convertServerDateTime2(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yy hh:mm:a", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException | NullPointerException e) {
            outputDateStr="Not available";
        }
        return outputDateStr;
    }

    // use in adapter Class
    public static CharSequence convertTimeStamp(String mileSecond){
        return convertTimeStamp(Long.parseLong(mileSecond));
    }

    public static CharSequence convertTimeStamp(Long mileSecond){
        return DateUtils.getRelativeTimeSpanString(mileSecond, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
    }


}
