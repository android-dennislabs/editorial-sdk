package com.editorial.util;

import android.content.Context;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.editorial.R;
import com.editorial.listeners.EditorialCallback;

public class TextActionModeCallback implements ActionMode.Callback {

    private final Context context;
    private final TextView textView;
    private final EditorialCallback.SelectionCallback callback;


    public TextActionModeCallback(Context context, TextView textView, EditorialCallback.SelectionCallback callback) {
        this.context = context;
        this.textView = textView;
        this.callback = callback;
    }


    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        // Remove All options
        menu.clear();
        // Remove the "select all" option
//        menu.removeItem(android.R.id.selectAll);
        // Remove the "cut" option
//        menu.removeItem(android.R.id.cut);
        // Remove the "copy all" option
//        menu.removeItem(android.R.id.copy);
        mode.getMenuInflater().inflate(R.menu.et_menu_text_selection, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        final CharSequence selectedText = ETTextEditor.getSelectedText(textView);
        if (item.getItemId() == R.id.custom_copy) {
            ETTextEditor.copyToClipBoard(context, selectedText);
            Toast.makeText(context, "Copied to clipboard", Toast.LENGTH_SHORT).show();
            mode.finish();
            return true;
        }
        else if (item.getItemId() == R.id.custom_add) {
            callback.onAddPointClick(selectedText);
            mode.finish();
            return true;
        }

        return false;
    }



    @Override
    public void onDestroyActionMode(ActionMode mode) {

    }

}