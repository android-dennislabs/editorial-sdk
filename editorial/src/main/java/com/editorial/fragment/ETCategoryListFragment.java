package com.editorial.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.editorial.EditorialSdk;
import com.editorial.R;
import com.editorial.activity.ETSubCatActivity;
import com.editorial.adapter.ETCategoryListAdapter;
import com.editorial.listeners.EditorialCallback;
import com.editorial.model.ETCategoryBean;
import com.editorial.model.ETCategoryProperty;
import com.editorial.network.ETNetworkUtil;
import com.editorial.util.ETConstant;
import com.editorial.util.ETLogger;
import com.editorial.util.EditorialUtil;
import com.editorial.util.database.ETDbHelper;
import com.helper.util.BaseUtil;

import java.util.ArrayList;


/**
 * Created by Amit on 1/12/2017.
 */

public class ETCategoryListFragment extends ETBaseFragment implements ETCategoryListAdapter.OnCustomClick {

    private ETDbHelper dbHelper;
    private RecyclerView mRecyclerView;
    private View view;
    private Activity activity;
    private int catId = 0, image;
    private ArrayList<ETCategoryBean> categoryBeen;
    private View llNoData;
    private ETCategoryProperty categoryProperty = null;
    private ETNetworkUtil networkUtil;
    private TextView tvNoData;
    private View pbLoader;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.et_fragment_category_list, container, false);
        activity = getActivity();
        initViews();
        initArguments();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isLoaded)
            loadData();
    }

    private boolean isLoaded = false;

    @Override
    public void onRefreshFragment() {
        if (!isLoaded) {
            loadData();
            isLoaded = true;
        }
    }


    private void initArguments() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.getSerializable(ETConstant.CAT_PROPERTY) != null && bundle.getSerializable(ETConstant.CAT_PROPERTY).getClass() == ETCategoryProperty.class) {
                categoryProperty = (ETCategoryProperty) bundle.getSerializable(ETConstant.CAT_PROPERTY);
                if (categoryProperty != null) {
                    catId = categoryProperty.getCatId();
                    image = categoryProperty.getImageResId();
                }
            }
        }
    }

    private void initObjects() {
        if(dbHelper ==null) {
            dbHelper = EditorialSdk.getInstance().getDBObject(activity);
        }
        if(networkUtil==null) {
            networkUtil = new ETNetworkUtil(activity);
        }
    }

    private void loadData() {
        if (catId == 0) {
            return;
        }
        initObjects();
        networkUtil.fetchCategoryListFragment(categoryProperty, catId, image, new EditorialCallback.CategoryCallback() {

            @Override
            public void onCategoryLoaded(ArrayList<ETCategoryBean> response, SparseArray<String> categoryNames, SparseArray<String> categoryImages) {
                categoryBeen = response;
                setUpList();
            }

            @Override
            public void onFailure(Exception e) {
                ETLogger.e(e.toString());
                showNoDataViews();
            }
        });
    }

    private void showNoDataViews() {
        BaseUtil.showNoData(llNoData, View.VISIBLE);
    }

    private void initViews() {
        mRecyclerView = view.findViewById(R.id.itemsRecyclerView);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(activity, 1);
        mRecyclerView.setLayoutManager(mLayoutManager);
        SwipeRefreshLayout swipeRefreshLayout = (view.findViewById(R.id.swipeRefreshLayout));
        swipeRefreshLayout.setEnabled(false);
        llNoData = view.findViewById(R.id.ll_no_data);
        tvNoData = view.findViewById(R.id.tv_no_data);
        pbLoader = view.findViewById(R.id.player_progressbar);
    }

    private void setUpList() {
        llNoData.setVisibility(View.GONE);
        RecyclerView.Adapter mAdapter = new ETCategoryListAdapter(activity, categoryBeen, this, R.layout.et_item_category_list);
//        mAdapter = new ETCategoryListAdapter( categoryBeen, this , R.layout.item_grid );
        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    public void onCustomItemClick(int position) {
        Intent intent = new Intent(activity, ETSubCatActivity.class);
        ETCategoryProperty catProperty = categoryProperty.getClone()
                .setCatId(catId)
                .setSubCatId(categoryBeen.get(position).getCategoryId())
                .setTitle(categoryBeen.get(position).getCategoryName())
                .setImageResId(image);
        if (!TextUtils.isEmpty(categoryBeen.get(position).getImageUrl())) {
            catProperty.setImageUrl(categoryBeen.get(position).getImageUrl());
        } else {
            catProperty.setImageUrl(categoryProperty.getImageUrl());
        }
        intent.putExtra(ETConstant.CAT_PROPERTY, catProperty);
        activity.startActivity(intent);
    }


}
