package com.editorial.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.config.config.ApiEndPoint;
import com.config.config.ConfigConstant;
import com.config.config.ConfigManager;
import com.editorial.EditorialSdk;
import com.editorial.R;
import com.editorial.activity.ETCategoryActivity;
import com.editorial.adapter.ETCategoryAdapter;
import com.editorial.listeners.EditorialCallback;
import com.editorial.model.ETCategoryBean;
import com.editorial.model.ETCategoryProperty;
import com.editorial.model.ETEditorialBean;
import com.editorial.model.ETServerBean;
import com.editorial.network.ETNetworkUtil;
import com.editorial.util.ETConstant;
import com.editorial.util.ETLogger;
import com.editorial.util.EditorialClassUtil;
import com.editorial.util.EditorialUtil;
import com.editorial.util.database.ETDBConstant;
import com.editorial.util.database.ETDbHelper;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.helper.task.TaskRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;


/**
 * Created by Amit on 1/12/2017.
 */

public class ETCategoryFragment extends ETBaseFragment implements ETCategoryAdapter.OnLoadMore, ETCategoryAdapter.OnClick, ETCategoryAdapter.OnNextLoad,
        SwipeRefreshLayout.OnRefreshListener, EditorialCallback.OnCustomResponse {

    private ETDbHelper dbHelper;
    private RecyclerView mRecyclerView;
    private ETCategoryAdapter mAdapter;
    private final ArrayList<ETEditorialBean> homeBeen = new ArrayList<>();
    private View llNoData;
    private View pbLoader;
    private TextView tvNoData;
    private View viewLoadMore;
    private boolean canLoadMore = true, isNotFavTab = true, isFirstHit = true;
    private View view;
    private Activity activity;
    private int image;
    private boolean isMoreIds = true, isNotification;
    private String[] ids;
    private int catId = 0, subCatID;
    private String query = "";
    private SwipeRefreshLayout swipeRefreshLayout;
    private final Handler handler = new Handler();
    private boolean isNetWorkCall = false;
    private ETCategoryProperty categoryProperty = null;
    private ETNetworkUtil networkUtil;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.et_fragment_category, container, false);
        activity = getActivity();
        networkUtil = new ETNetworkUtil(activity);
        initDataFromArg();
        initViews();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (isLoaded)
            loadData();
    }

    private boolean isLoaded = false;

    @Override
    public void onRefreshFragment() {
        if (!isLoaded) {
            loadData();
            isLoaded = true;
        } else {
            if (hashMapCategoryNames == null && homeBeen != null && homeBeen.size() > 0) {
                initCategoryNames();
                setupList();
            }
        }
    }

    public void loadData() {
        initCategoryNames();
        requestDataFromDB();
    }

    private void initDataFromArg() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.getSerializable(ETConstant.CAT_PROPERTY) != null && bundle.getSerializable(ETConstant.CAT_PROPERTY).getClass() == ETCategoryProperty.class) {
                categoryProperty = (ETCategoryProperty) bundle.getSerializable(ETConstant.CAT_PROPERTY);

                if (categoryProperty != null) {
                    catId = categoryProperty.getCatId();
                    subCatID = categoryProperty.getSubCatId();
                    image = categoryProperty.getImageResId();
                    isNotification = categoryProperty.isNotification();
                    if (catId != 0)
                        query = categoryProperty.getQuery();
                    if (query == null)
                        query = "";
                    isNotFavTab = !query.contains(ETDbHelper.COLUMN_FAV);

                    isLoaded = categoryProperty.isLoadUI();
                    if (isLoaded) {
                        loadData();
                    }
                }

            }
        }
        dbHelper = EditorialSdk.getInstance().getDBObject(activity);
    }

    private SparseArray<String> hashMapCategoryNames = null;
    private SparseArray<String> hashMapCategoryImages = null;

    private void initCategoryNames() {
        if (isVisible())
            networkUtil.fetchCategoryListFragment(categoryProperty, catId, image, new EditorialCallback.CategoryCallback() {

                @Override
                public void onCategoryLoaded(ArrayList<ETCategoryBean> categoryBeen, SparseArray<String> categoryNames, SparseArray<String> categoryImages) {
                    hashMapCategoryNames = categoryNames;
                    hashMapCategoryImages = categoryImages;
                    if (mAdapter != null) {
                        mAdapter.setCategoryData(hashMapCategoryNames, hashMapCategoryImages);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        setupList();
                    }
                }

                @Override
                public void onFailure(Exception e) {
                    ETLogger.e(e.toString());
                    showNoDataViews();
                }
            });
    }

    private void initViews() {
        if (view != null) {
            mRecyclerView = view.findViewById(R.id.itemsRecyclerView);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
            swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
            swipeRefreshLayout.setOnRefreshListener(this);
            llNoData = view.findViewById(R.id.ll_no_data);
            viewLoadMore = view.findViewById(R.id.ll_load_more);
            tvNoData = view.findViewById(R.id.tv_no_data);
            tvNoData.setText("Processing, Please wait...");
            pbLoader = view.findViewById(R.id.player_progressbar);
        }
    }

    private void setupList() {
        mAdapter = new ETCategoryAdapter(homeBeen, this, isNotFavTab ? this : null, isNotFavTab ? this : null, activity, hashMapCategoryNames, hashMapCategoryImages);
        mAdapter.setImageRes(image);
        if (categoryProperty != null) {
            mAdapter.setImageUrl(categoryProperty.getImageUrl());
        }
        mAdapter.setCategoryProperty(categoryProperty);
//        mAdapter.setTypePdf(isTypePdf);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onCustomItemClick(final int position, String category, int type) {
        if (type == ETCategoryAdapter.OnClick.TYPE_ARTICLE) {
            ETEditorialBean bean = homeBeen.get(position);
            bean.setCategory(category);
            EditorialClassUtil.openEditorialActivity(activity, bean, catId, query, categoryProperty.getHost());
        } else {
            updateFavStatus(position);
        }
    }

    private void updateFavStatus(final int position) {
        if (homeBeen.size() > 0) {
            final boolean status = homeBeen.get(position).isFav();
            homeBeen.get(position).setFav(!status);
            mAdapter.notifyDataSetChanged();
            EditorialUtil.showToastCentre(activity, status ? " Removed " : " Saved ");
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    dbHelper.updateArticle(homeBeen.get(position).getId(), ETDBConstant.COLUMN_FAV,
                            status ? ETDBConstant.INT_FALSE : ETDBConstant.INT_TRUE, catId);
                    return null;
                }
            });
            try {
                ETCategoryActivity categoryActivity = (ETCategoryActivity) activity;
                if (categoryActivity != null) {
                    categoryActivity.updateFragmentList(!isNotFavTab);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCustomLoadMore() {
        if (EditorialUtil.isConnected(activity)) {
            loadMoreData();
        }
    }

    public void refreshFragment() {
        requestDataFromDB();
    }

    @Override
    public void onRefresh() {
        if (isNotFavTab && !isNotification)
            fetchLatestData(true);
    }

    private void fetchLatestData(final boolean isLatest) {
        long id = (isLatest) ? ETConstant.MAX_VALUE : homeBeen.get(homeBeen.size() - 1).getId();
        if (!isNetWorkCall) {
            if (swipeRefreshLayout != null) {
                swipeRefreshLayout.setRefreshing(true);
            }
            isNetWorkCall = true;
            Map<String, String> map = new HashMap<>(2);
            map.put("id", getCatId() + "");
            map.put("max_content_id", id + "");
            EditorialSdk.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, categoryProperty.getHost(), ApiEndPoint.GET_PREVIOUS_CONTENT_BY_CAT_ID
                    , map, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            ETLogger.e("fetchLatestData");
                            isFirstHit = false;
                            if (status && !EditorialUtil.isEmptyOrNull(data)) {
                                List<ETServerBean> list = null;
                                try {
                                    list = ConfigManager.getGson().fromJson(data, new TypeToken<List<ETServerBean>>() {
                                    }.getType());
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                }

                                ETLogger.e("fetchLatestData after convert");
                                if (list != null && list.size() > 0) {
                                    requestDataInsert(list, isCatDelete(isLatest));
                                    if (isCatDelete(isLatest) && activity instanceof ETCategoryActivity)
                                        ((ETCategoryActivity) activity).fetchLatestIds(ETConstant.MAX_VALUE);
                                    onCustomResponse(true);
                                } else {
                                    if (!isLatest)
                                        onCustomResponse(false);
                                    else {
                                        showNoData();
                                    }

                                }
                            } else
                                showNoData();
                            isNetWorkCall = false;
                            if (swipeRefreshLayout != null) {
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        }
                    });
        }
    }

    private void showNoData() {
        if (!isFirstHit && (homeBeen == null || homeBeen.size() < 1)) {
            showNoDataViews();
        }
    }

    private boolean isCatDelete(boolean isLat) {
        return isLat && (activity instanceof ETCategoryActivity);
    }

    private void fetchArticleForId(String[] ids) {
        if (!isNetWorkCall) {
            isNetWorkCall = true;
            Map<String, String> map = new HashMap<>(1);
            map.put("id_array", toString(ids));
            EditorialSdk.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, categoryProperty.getHost()
                    , ApiEndPoint.GET_CONTENT_BY_PRODUCT_IDS_ARRAY
                    , map, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            if (status && !EditorialUtil.isEmptyOrNull(data)) {
                                List<ETServerBean> list = null;
                                try {
                                    list = ConfigManager.getGson().fromJson(data, new TypeToken<List<ETServerBean>>() {
                                    }.getType());
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                }

                                if (list != null && list.size() > 0) {
                                    requestDataInsert(list, false);
                                    onCustomResponse(true);
                                } else
                                    onCustomResponse(false);
                            } else
                                onCustomResponse(false);

                            isNetWorkCall = false;
                        }
                    });
        }
    }

    private void loadMoreData() {
        if (EditorialUtil.isConnected(activity)) {
            if (!isNetWorkCall && viewLoadMore != null && viewLoadMore.getVisibility() == View.GONE && canLoadMore) {
                showView(viewLoadMore);
//                new fetchIds( true ).execute();
                manageMoreData();
            }
        } else {
            hideView(viewLoadMore);
        }
    }

    private void manageMoreData() {
        if (!isNetWorkCall) {
            if (ids != null && ids.length > 0 && isMoreIds) {
                fetchArticleForId(ids);
            } else {
                isMoreIds = false;
                fetchLatestData(false);
            }
        } else {
            hideView(viewLoadMore);
        }
    }

    private void hideView(View view) {
        if (view != null) {
            view.setVisibility(View.GONE);
        }
    }

    private void showView(View view) {
        if (view != null) {
            view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCustomResponse(boolean result) {
        hideView(viewLoadMore);
        if (!isMoreIds)
            canLoadMore = result;
    }

    @Override
    public void onNextLoad() {
        manageMoreData();
    }

    private void requestDataFromDB(){
        TaskRunner.getInstance().executeAsync(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (dbHelper != null) {
                    dbHelper.callDBFunction(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            dbHelper.fetchHomeData(homeBeen, query, catId, isNotFavTab);
                            if (!(isNotFavTab && isFirstHit && !isNotification))
                                getMoreIds();
                            return null;
                        }
                    });
                }
                return null;
            }
        }, new TaskRunner.Callback<Void>() {
            @Override
            public void onComplete(Void result) {
                if (isNotFavTab && isFirstHit && !isNotification)
                    fetchLatestData(true);
                handleData();
            }
        });
    }

    public void callIdFunc() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                requestFetchIds(false);
            }
        }, 0);
    }

    private void requestDataInsert(final List<ETServerBean> idBeen, final boolean isDelete){
        TaskRunner.getInstance().executeAsync(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                final ETDbHelper dbHelper = EditorialSdk.getInstance().getDBObject(activity);
                dbHelper.callDBFunction(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        dbHelper.insertEditorial(idBeen, catId, isDelete);
                        dbHelper.fetchHomeData(homeBeen, query, catId, isNotFavTab);
                        getMoreIds();
                        return null;
                    }
                });
                return null;
            }
        }, new TaskRunner.Callback<Void>() {
            @Override
            public void onComplete(Void result) {
                handleData();
            }
        });
    }

    private void getMoreIds() {
        if (isMoreIds) {
            ids = dbHelper.fetchLatestEmptyIds(query, catId, activity);
            if (mAdapter != null) {
                if (ids != null && ids.length > 0)
                    mAdapter.setMaxId(Integer.parseInt(ids[0]));
                else
                    mAdapter.setMaxId(0);
            }
        }
    }

    private void handleData() {
        if (llNoData != null && mRecyclerView != null) {
            if (homeBeen.size() > 0) {
                if (mAdapter == null) {
                    setupList();
                }
                llNoData.setVisibility(View.GONE);
                mRecyclerView.getRecycledViewPool().clear();
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });
            } else if (!isFirstHit || !isNotFavTab) {
                showNoDataViews();
            }
        }
    }

    private void showNoDataViews() {
        if (homeBeen.size() < 1) {
            showView(llNoData);
            showView(tvNoData);
            if (tvNoData != null) {
                if (EditorialUtil.isConnected(activity)) {
                    tvNoData.setText(ETConstant.NO_DATA);
                } else {
                    tvNoData.setText(ETConstant.NO_INTERNET);
                }
            }
            hideView(pbLoader);
        }
    }


    private int getCatId() {
        return subCatID == 0 ? catId : subCatID;
    }

    private String toString(String[] a) {
        if (a == null)
            return "null";

        int iMax = a.length - 1;
        if (iMax == -1)
            return "[]";

        StringBuilder b = new StringBuilder();
        b.append('[');
        for (int i = 0; ; i++) {
            b.append(a[i]);
            if (i == iMax)
                return b.append(']').toString();
            b.append(",");
        }
    }

    private void requestFetchIds(final boolean isNetWorkUpdate){
        TaskRunner.getInstance().executeAsync(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (isMoreIds) {
                    final ETDbHelper dbHelper = EditorialSdk.getInstance().getDBObject(activity);
                    dbHelper.callDBFunction(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            ids = dbHelper.fetchLatestEmptyIds(query, catId, activity);
                            if (mAdapter != null) {
                                if (ids != null && ids.length > 0)
                                    mAdapter.setMaxId(Integer.parseInt(ids[0]));
                                else
                                    mAdapter.setMaxId(0);
                            }
                            return null;
                        }
                    });
                }
                return null;
            }
        }, new TaskRunner.Callback<Void>() {
            @Override
            public void onComplete(Void result) {
                if (isNetWorkUpdate) {
                    if (ids != null && ids.length > 0 && isMoreIds) {
                        fetchArticleForId(ids);
                    } else {
                        isMoreIds = false;
                        fetchLatestData(false);
                    }
                } else {
                    hideView(viewLoadMore);
                }
            }
        });
    }
}
