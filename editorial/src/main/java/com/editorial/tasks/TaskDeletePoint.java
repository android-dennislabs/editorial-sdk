package com.editorial.tasks;

import android.content.Context;

import com.editorial.EditorialSdk;
import com.editorial.listeners.EditorialCallback;
import com.editorial.model.ETEditorialBean;
import com.editorial.util.database.ETDbHelper;
import com.helper.task.TaskRunner;

import java.util.concurrent.Callable;

public class TaskDeletePoint{

    private final EditorialCallback.Response<Integer> listener;
    private final int position;
    private final ETEditorialBean item;
    private Context context;
    private ETDbHelper dbHelper;


    public TaskDeletePoint(Context context, int position, ETEditorialBean item, EditorialCallback.Response<Integer> listener) {
        this.context = context;
        this.position = position;
        this.item = item;
        this.listener = listener;
    }

    public void execute() {
        dbHelper = EditorialSdk.getInstance().getDBObject(context);
        TaskRunner.getInstance().executeAsync(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dbHelper.callDBFunction(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        dbHelper.deletePoint(item);
                        return null;
                    }
                });
                return null;
            }
        }, new TaskRunner.Callback<Void>() {
            @Override
            public void onComplete(Void result) {
                listener.onSuccess(position);
            }
        });
    }
}