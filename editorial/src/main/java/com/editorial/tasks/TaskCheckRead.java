//package com.editorial.tasks;
//
//import android.content.Context;
//import android.os.AsyncTask;
//
//import com.editorial.EditorialSdk;
//import com.editorial.listeners.EditorialCallback;
//import com.editorial.model.ETVocubModel;
//import com.editorial.util.database.ETDbHelper;
//
//import java.util.concurrent.Callable;
//
//public class TaskCheckRead extends AsyncTask<Void, Void, Void> {
//
//    private final int id,catId;
//    private Context context;
//    private ETDbHelper dbHelper;
//
//
//    public TaskCheckRead(Context context, int id, int catId) {
//        this.context = context;
//        this.id = id;
//        this.catId = catId;
//    }
//
//    @Override
//    protected void onPreExecute() {
//        super.onPreExecute();
//        dbHelper = EditorialSdk.getInstance().getDBObject(context);
//
//    }
//
//    @Override
//    protected Void doInBackground(Void... voids) {
//        dbHelper.callDBFunction(new Callable<Void>() {
//            @Override
//            public Void call() throws Exception {
//                dbHelper.updateArticle(id, ETDbHelper.COLUMN_READ, ETDbHelper.INT_TRUE, catId);
//                return null;
//            }
//        });
//        return null;
//    }
//}