//package com.editorial.tasks;
//
//import android.content.Context;
//import android.os.AsyncTask;
//
//import com.editorial.EditorialSdk;
//import com.editorial.listeners.EditorialCallback;
//import com.editorial.model.ETEditorialBean;
//import com.editorial.util.database.ETDbHelper;
//
//import java.util.concurrent.Callable;
//
//public class DataInsertPoints extends AsyncTask<Void, Void, Void> {
//
//    private final EditorialCallback.Response<String> listener;
//    private Context context;
//    private ETEditorialBean mItem;
//    private ETEditorialBean mEditorial;
//    private CharSequence word;
//    private int catId;
//    private ETDbHelper dbHelper;
//
//
//    public DataInsertPoints(Context context, ETEditorialBean mItem, ETEditorialBean mEditorial, CharSequence word, int catId, EditorialCallback.Response<String> listener) {
//        this.context = context;
//        this.mItem = mItem;
//        this.mEditorial = mEditorial;
//        this.word = word;
//        this.catId = catId;
//        this.listener = listener;
//    }
//
//    @Override
//    protected void onPreExecute() {
//        super.onPreExecute();
//        dbHelper = EditorialSdk.getInstance().getDBObject(context);
//
//    }
//
//    @Override
//    protected Void doInBackground(Void... voids) {
//        dbHelper.callDBFunction(new Callable<Void>() {
//            @Override
//            public Void call() throws Exception {
//                dbHelper.insertPoint(mItem, mEditorial, word, catId);
//                return null;
//            }
//        });
//        return null;
//    }
//
//    @Override
//    protected void onPostExecute(Void aVoid) {
//        super.onPostExecute(aVoid);
//        listener.onSuccess("Success");
//    }
//}