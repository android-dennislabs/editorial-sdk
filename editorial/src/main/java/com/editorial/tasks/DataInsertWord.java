//package com.editorial.tasks;
//
//import android.content.Context;
//import android.os.AsyncTask;
//
//import com.editorial.EditorialSdk;
//import com.editorial.listeners.EditorialCallback;
//import com.editorial.util.database.ETDbHelper;
//
//import java.util.concurrent.Callable;
//
//public class DataInsertWord extends AsyncTask<Void, Void, Void> {
//
//    private final EditorialCallback.Response<String> listener;
//    private Context context;
//    private CharSequence word, meaning;
//    private ETDbHelper dbHelper;
//
//
//    public DataInsertWord(Context context, CharSequence word, CharSequence meaning, EditorialCallback.Response<String> listener) {
//        this.context = context;
//        this.word = word;
//        this.meaning = meaning;
//        this.listener = listener;
//    }
//
//    @Override
//    protected void onPreExecute() {
//        super.onPreExecute();
//        dbHelper = EditorialSdk.getInstance().getDBObject(context);
//
//    }
//
//    @Override
//    protected Void doInBackground(Void... voids) {
//        dbHelper.callDBFunction(new Callable<Void>() {
//            @Override
//            public Void call() throws Exception {
//                dbHelper.insertWord(word, meaning);
//                return null;
//            }
//        });
//        return null;
//    }
//
//    @Override
//    protected void onPostExecute(Void aVoid) {
//        super.onPostExecute(aVoid);
//        listener.onSuccess("Success");
//    }
//}