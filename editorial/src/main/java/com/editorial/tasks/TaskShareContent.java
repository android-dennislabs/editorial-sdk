package com.editorial.tasks;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;

import com.editorial.R;
import com.editorial.util.DateTimeUtil;
import com.editorial.util.ETConstant;
import com.editorial.util.EditorialUtil;
import com.helper.task.TaskRunner;
import com.helper.util.BaseUtil;
import com.helper.util.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.Callable;

public class TaskShareContent{

    private final View view;
    private final Context context;
    private final WindowManager windowManager;
    private final String contentFileName;
    private final int catId,clickedId;
    private final String description;


    public TaskShareContent(View view,int catId, int clickedId, String description, WindowManager windowManager) {
        this.view = view;
        this.catId = catId;
        this.clickedId = clickedId;
        this.description = description;
        this.context = view.getContext();
        this.windowManager = windowManager;
        this.contentFileName = ETConstant.DOWNLOAD_DIRECTORY +"-"+ DateTimeUtil.getTimeStamp();
    }

    public void execute() {
        BaseUtil.showDialog(context, "Creating file", true);
        TaskRunner.getInstance().executeAsync(new Callable<File>() {
            @Override
            public File call() throws Exception {
                if (Build.VERSION.SDK_INT > 18) {
                    return saveViewToPDF();
                } else {
                    return null;
                }
            }
        }, new TaskRunner.Callback<File>() {
            @Override
            public void onComplete(File file) {
                BaseUtil.hideDialog();
                if(file!=null){
                    share(file);
                }else {
                    EditorialUtil.shareArticle(context, getStringDesc(), catId, clickedId);
                }
            }
        });
    }

    private String getStringDesc() {
        String s;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            s = Html.fromHtml(description, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            s = Html.fromHtml(description).toString();
        }
        return s;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private File saveViewToPDF() {
        File mFile = null;
        try {
            if (view != null) {
                Display display = windowManager.getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int height = size.y;
                Bitmap bitmap = getScreenShot(view);

                final String fileName = contentFileName + ".png";
                File sourceDir = FileUtils.getFileStoreDirectoryPublic(context);
                if (!sourceDir.exists()) {
                    sourceDir.mkdir();
                }
                File file = new File(sourceDir, fileName);

                FileOutputStream fOut = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                fOut.flush();
                fOut.close();
                mFile = file;
            }
//            SupportUtil.share(DescActivity.this, Uri.fromFile(file));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mFile;
    }

    private void share(File file) {
//        String strUri = EditorialUtil.getArticleUri(catId, clickedId);
        String strUri = EditorialUtil.getAppDownloadLink(context);
        Uri fileUri = FileProvider.getUriForFile(context, context.getPackageName() + context.getString(R.string.file_provider), file);
        EditorialUtil.share(context, fileUri, strUri);
    }


    private static Bitmap getScreenShot(View v) {
        Bitmap b = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);
        return b;
    }


}