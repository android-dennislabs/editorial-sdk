//package com.editorial.tasks;
//
//import android.content.Context;
//import android.os.AsyncTask;
//
//import com.editorial.EditorialSdk;
//import com.editorial.listeners.EditorialCallback;
//import com.editorial.model.ETEditorialBean;
//import com.editorial.model.ETVocubModel;
//import com.editorial.util.database.ETDbHelper;
//
//import java.util.concurrent.Callable;
//
//public class TaskDeleteWord extends AsyncTask<Void, Void, Void> {
//
//    private final EditorialCallback.Response<Integer> listener;
//    private final int position;
//    private final ETVocubModel item;
//    private Context context;
//    private ETDbHelper dbHelper;
//
//
//    public TaskDeleteWord(Context context, int position, ETVocubModel item, EditorialCallback.Response<Integer> listener) {
//        this.context = context;
//        this.position = position;
//        this.item = item;
//        this.listener = listener;
//    }
//
//    @Override
//    protected void onPreExecute() {
//        super.onPreExecute();
//        dbHelper = EditorialSdk.getInstance().getDBObject(context);
//
//    }
//
//    @Override
//    protected Void doInBackground(Void... voids) {
//        dbHelper.callDBFunction(new Callable<Void>() {
//            @Override
//            public Void call() throws Exception {
//                dbHelper.deleteWord(item);
//                return null;
//            }
//        });
//        return null;
//    }
//
//    @Override
//    protected void onPostExecute(Void aVoid) {
//        super.onPostExecute(aVoid);
//        listener.onSuccess(position);
//    }
//}