package com.editorial.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.adssdk.NativePagerAdapter;
import com.adssdk.util.AdsUtil;
import com.editorial.EditorialSdk;
import com.editorial.R;
import com.editorial.listeners.EditorialCallback;
import com.editorial.listeners.EditorialMode;
import com.editorial.model.ETEditorialBean;
import com.editorial.util.ETPreferences;
import com.editorial.util.ETTextEditor;
import com.editorial.util.EditorialUtil;

import java.util.ArrayList;

/**
 * Created by amit on 18/9/16.
 */
public class EditorialAdapter extends NativePagerAdapter {

    private final EditorialCallback.SelectionCallback textSelectionCallback;
    private final Typeface typeface;
    private SparseArray<String> hashMapCategoryNames;
    private LayoutInflater inflater;
    private ArrayList<ETEditorialBean> mList;
    private Activity context;
    private TextView tvTitle, tvEditorial, tvCategory, tvTag, tvDate;
    private LinearLayout layoutEditorial;
    private RelativeLayout rlAds;
    private EditorialCallback.WordListener wordListener;
    private ETTextEditor editor;
    private boolean isDayMode = true;
    private EditorialMode editorialMode;

    public EditorialAdapter(Activity context, ArrayList<ETEditorialBean> articleBeans, ETTextEditor editor, SparseArray<String> hashMapCategoryNames, EditorialCallback.WordListener wordListener, EditorialCallback.SelectionCallback textSelectionCallback) {
        super(articleBeans, R.layout.native_pager_ad_app_install, context);
        this.context = context;
        this.mList = articleBeans;
        this.wordListener = wordListener;
        this.hashMapCategoryNames = hashMapCategoryNames;
        this.editor = editor;
        this.textSelectionCallback = textSelectionCallback;
        inflater = LayoutInflater.from(context);
        this.typeface = EditorialSdk.getInstance().getTypeface(context);

    }


    @Override
    protected View customInstantiateItem(ViewGroup container, int position) {
        final ETEditorialBean bean = mList.get(position);
        View view = null;
        try {
            view = inflater.inflate(R.layout.et_layout_editorial_item, container, false);
            layoutEditorial = view.findViewById(R.id.layout_editorial);
            rlAds = (RelativeLayout) view.findViewById(R.id.ll_ad);
            tvTitle = view.findViewById(R.id.tv_title);
            tvCategory = view.findViewById(R.id.tv_category);
            tvTag = view.findViewById(R.id.tv_tag);
            tvDate = view.findViewById(R.id.tv_date);
            tvEditorial = view.findViewById(R.id.tv_desc);

//            loadFullAd(rlAds);
            EditorialUtil.loadNativeAd(context , rlAds,false,R.layout.ads_native_unified_card);

            setHeaderData(bean);
            updateBodyData(bean);
            updateTheme(view);
 
            view.setTag("pager_item" + position);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        container.addView(view);
        return view;
    }

    private void updateTheme(View view) {
        view.setBackgroundColor(isDayMode ? Color.WHITE : ContextCompat.getColor(context, R.color.themeWindowBackground));
        layoutEditorial.setBackgroundColor(isDayMode ? Color.WHITE : ContextCompat.getColor(context, R.color.themeWindowBackground));
        tvTitle.setTextColor(isDayMode ? ContextCompat.getColor(context, R.color.themeTextColor) : Color.WHITE);
        tvCategory.setBackgroundResource(isDayMode ? R.drawable.bg_et_item_category_lite : R.drawable.bg_et_item_category_dark);
        tvDate.setTypeface(typeface);
        tvTitle.setTypeface(typeface);
        tvCategory.setTypeface(typeface);
        tvTag.setTypeface(typeface);
        tvEditorial.setTypeface(typeface);
    }

    private void setHeaderData(ETEditorialBean mItem) {
        tvTitle.setText(mItem.getTitle());
        String cat = "";
        if (hashMapCategoryNames != null && hashMapCategoryNames.size() > 0) {
            cat= hashMapCategoryNames.get(mItem.getSubCatId());
        }
        if (!TextUtils.isEmpty(cat)) {
            tvCategory.setText(cat);
        } else {
            tvCategory.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(mItem.getTag())) {
            Spanned tag = Html.fromHtml(mItem.getTag());
            tvTag.setText(tag.toString().replaceAll("\\n", ""));
        } else {
            tvTag.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(mItem.getDate())) {
            tvDate.setText(mItem.getDate());
        } else {
            tvDate.setVisibility(View.GONE);
        }
    }

    private void updateBodyData(ETEditorialBean mEditorial) {
        tvEditorial.setVisibility(View.VISIBLE);
        tvEditorial.setTextIsSelectable(false);
        editor.setClickableString(editorialMode, tvEditorial, mEditorial.getDesc(), isDayMode, wordListener , textSelectionCallback);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }


    public void setDayMode(boolean isDayMode) {
        this.isDayMode = isDayMode;
    }


    public void setEditorialMode(EditorialMode mode) {
        this.editorialMode = mode;
    }


    public void share(int position, int catId, WindowManager windowManager) {
        editor.share(layoutEditorial, catId, mList.get(position).getId(), mList.get(position).getDesc(), windowManager);
    }


    public void setCategoryData(SparseArray<String> hashMapCategoryNames) {
        this.hashMapCategoryNames = hashMapCategoryNames;
    }



}
