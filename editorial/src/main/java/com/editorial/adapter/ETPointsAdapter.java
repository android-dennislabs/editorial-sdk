package com.editorial.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.adssdk.adapter.NativeAdsListAdapter;
import com.config.config.ConfigConstant;
import com.editorial.EditorialSdk;
import com.editorial.R;
import com.editorial.model.ETEditorialBean;
import com.editorial.util.EditorialClassUtil;

import java.util.ArrayList;


/**
 * Created by Amit on 1/10/2017.
 */

public class ETPointsAdapter extends NativeAdsListAdapter {

    private final Typeface typeface;
    private final OnClick onClick;
    private final Activity activity;
    private ArrayList<ETEditorialBean> homeBeen;

    public interface OnClick {
        void onDelete(int position, ETEditorialBean item);
    }

    public ETPointsAdapter(ArrayList<ETEditorialBean> homeBeen, Activity activity) {
        super(activity , homeBeen, R.layout.ads_native_unified_card, null);
        this.homeBeen = homeBeen;
        this.activity = activity;
        this.onClick = (OnClick) activity;
        typeface = EditorialSdk.getInstance().getTypeface(activity);
    }

    @Override
    protected RecyclerView.ViewHolder onAbstractCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.et_item_points, parent, false);
        return new ArticleViewHolder(view);
    }

    @Override
    protected void onAbstractBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArticleViewHolder) {
            ArticleViewHolder viewHolder = (ArticleViewHolder) holder;
            if (homeBeen.size() > position) {
                final ETEditorialBean bean = homeBeen.get(position);
                viewHolder.tvTitle.setText(bean.getTitle());
                viewHolder.tvDesc.setText(bean.getDesc());

                if (!TextUtils.isEmpty(bean.getCategory())) {
                    viewHolder.tvCategory.setText(bean.getCategory());
                    viewHolder.tvCategory.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.tvCategory.setVisibility(View.GONE);
                }
                if (!TextUtils.isEmpty(bean.getTag())) {
                    Spanned tag = Html.fromHtml(bean.getTag());
                    viewHolder.tvTag.setText(tag.toString().replaceAll("\\n", ""));
                    viewHolder.tvTag.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.tvTag.setVisibility(View.GONE);
                }
                if (!TextUtils.isEmpty(bean.getDate())) {
                    viewHolder.tvDate.setText(bean.getDate());
                    viewHolder.tvDate.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.tvDate.setVisibility(View.GONE);
                }
                final int pos = position;
                viewHolder.llDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onClick.onDelete(pos, bean);
                    }
                });
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditorialClassUtil.openEditorialActivity(activity, bean, 0, "", ConfigConstant.HOST_TRANSLATOR);
                    }
                });
            }
        }

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(View itemView) {
            super(itemView);
        }
    }

    class ArticleViewHolder extends ViewHolder {

        private TextView tvTitle, tvDesc, tvCategory, tvTag, tvDate;
        private LinearLayout llDelete;

        ArticleViewHolder(View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.item_tv_date);
            tvTitle = itemView.findViewById(R.id.item_tv_title);
            tvDesc = itemView.findViewById(R.id.item_tv_desc);
            tvCategory = itemView.findViewById(R.id.item_tv_category);
            tvTag = itemView.findViewById(R.id.item_tv_tag);
            llDelete = itemView.findViewById(R.id.ll_item_delete);
            tvTitle.setTypeface(typeface);
            tvCategory.setTypeface(typeface);
            tvDate.setTypeface(typeface);
            tvDesc.setTypeface(typeface);
        }

    }
}
