package com.editorial.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.adssdk.OnCustomLoadMore;
import com.adssdk.adapter.NativeAdsListAdapter;
import com.editorial.EditorialSdk;
import com.editorial.R;
import com.editorial.model.ETCategoryProperty;
import com.editorial.model.ETEditorialBean;
import com.editorial.util.ETPreferences;
import com.editorial.util.EditorialUtil;

import java.util.ArrayList;


/**
 * Created by Amit on 1/10/2017.
 */

public class ETCategoryAdapter extends NativeAdsListAdapter {

    private SparseArray<String> hashMapCategoryNames;
    private SparseArray<String> categoryImages;
    private ArrayList<ETEditorialBean> homeBeen;
    private OnClick onClick;
    private final OnLoadMore onLoadMore;
    private OnNextLoad onNextLoad;
    //    private HashMap<Integer, String> categoryNames;
    private int imageRes = 0;
    private Context context;
    private int maxId = 0;
    private boolean isDate = false, isTypePdf = false;
    private Typeface typeface;
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setTypePdf(boolean typePdf) {
        isTypePdf = typePdf;
    }

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }

    public ETCategoryAdapter(ArrayList<ETEditorialBean> homeBeen, OnClick onClick, final OnLoadMore onLoadMore, OnNextLoad onNextLoad, Activity context, SparseArray<String> hashMapCategoryNames, SparseArray<String> categoryImages) {
        super(context , homeBeen, R.layout.ads_native_unified_card, new OnCustomLoadMore() {
            @Override
            public void onLoadMore() {
                if (onLoadMore != null)
                    onLoadMore.onCustomLoadMore();
            }
        });
        this.homeBeen = homeBeen;
        this.onClick = onClick;
        this.context = context;
        this.onLoadMore = onLoadMore;
        this.onNextLoad = onNextLoad;
//        categoryNames = AppData.getInstance().getCategoryNames();
        this.hashMapCategoryNames = hashMapCategoryNames;
        this.categoryImages = categoryImages;
        this.typeface = EditorialSdk.getInstance().getTypeface(context);
    }

    public void setCategoryProperty(ETCategoryProperty categoryProperty) {
        if (categoryProperty != null) {
            isDate = categoryProperty.isDate();
        }
    }

    public void setCategoryData(SparseArray<String> hashMapCategoryNames, SparseArray<String> hashMapCategoryImages) {
        this.hashMapCategoryNames = hashMapCategoryNames;
        this.categoryImages = hashMapCategoryImages;
    }

    public interface OnClick {
        int TYPE_ARTICLE = 0;
        int TYPE_BOOKMARK = 1;

        void onCustomItemClick(int position, String category, int type);
    }

    public interface OnLoadMore {
        void onCustomLoadMore();
    }

    public void setMaxId(int maxId) {
        this.maxId = maxId;
    }

    public interface OnNextLoad {
        void onNextLoad();
    }

    @Override
    protected RecyclerView.ViewHolder onAbstractCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                isTypePdf ? R.layout.et_item_editorial : R.layout.et_item_editorial, parent, false);
        return new ArticleViewHolder(view, onClick);
    }

    @Override
    protected void onAbstractBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArticleViewHolder) {
            ArticleViewHolder viewHolder = (ArticleViewHolder) holder;
            if (homeBeen.size() > position) {
                ETEditorialBean bean = homeBeen.get(position);
                viewHolder.position = position;
                viewHolder.tvTitle.setText(bean.getTitle());
                String cat = "";
                if (hashMapCategoryNames != null && hashMapCategoryNames.size() > 0) {
                    cat= hashMapCategoryNames.get(bean.getSubCatId());
                }

                if(categoryImages!=null && categoryImages.size()>0){
                    String catImage = categoryImages.get(bean.getSubCatId());
                    if (!EditorialUtil.isEmptyOrNull(catImage)) {
                        EditorialUtil.loadUserImage(catImage, EditorialUtil.getMockImageUrl(), viewHolder.ivImage, R.drawable.exam_place_holder);
                    } else {
                        loadDefaultImage(viewHolder.ivImage);
                    }
                }else {
                    loadDefaultImage(viewHolder.ivImage);
                }

                if (!TextUtils.isEmpty(cat)) {
                    viewHolder.tvCategory.setText(cat);
                    viewHolder.tvCategory.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.tvCategory.setVisibility(View.GONE);
                }
                if (isDate && !TextUtils.isEmpty(bean.getDate())) {
                    viewHolder.tvDate.setText(bean.getDate());
                    viewHolder.tvDate.setVisibility(View.VISIBLE);
                }else {
                    viewHolder.tvDate.setVisibility(View.GONE);
                }

                if(ETPreferences.isDayMode(context)) {
                    viewHolder.tvTitle.setTextColor(bean.isTrue() ? Color.GRAY : Color.BLACK);
                }else {
                    viewHolder.tvTitle.setTextColor(bean.isTrue() ? Color.GRAY : Color.WHITE);
                }
                viewHolder.ivFav.setImageResource(bean.isFav() ? R.drawable.ic_bookmark_fill_dark : R.drawable.ic_bookmark_not_fill_dark);

                if (maxId > bean.getId() && onNextLoad != null)
                    onNextLoad.onNextLoad();
            }
        }

    }

    private void loadDefaultImage(ImageView mainImage) {
        if (EditorialUtil.isEmptyOrNull(imageUrl)) {
            mainImage.setImageResource(imageRes);
        } else {
            EditorialUtil.loadUserImage(imageUrl, EditorialUtil.getMockImageUrl(), mainImage, R.drawable.exam_place_holder);
        }
//        if (!EditorialUtil.isEmptyOrNull(imageUrl)) {
//            AppApplication.getInstance().getPicasso().load(imageUrl)
//                    .placeholder(R.drawable.exam_place_holder).into(mainImage);
//        } else if (imageRes != 0) {
//            mainImage.setImageResource(imageRes);
//        }else {
//            mainImage.setImageResource(R.drawable.exam_place_holder);
//        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    class ArticleViewHolder extends ViewHolder implements View.OnClickListener {

        private TextView tvTitle, tvCategory, tvDate;
        private ImageView ivImage;
        private int position;
        private OnClick onClick;
        private ImageView ivFav;
        private LinearLayout llSave;

        ArticleViewHolder(View itemView, OnClick onClick) {
            super(itemView);
            this.onClick = onClick;
            tvTitle = itemView.findViewById(R.id.item_tv_title);
            tvDate = itemView.findViewById(R.id.item_tv_date);
            tvCategory = itemView.findViewById(R.id.item_tv_category);
            ivImage = itemView.findViewById(R.id.item_iv_main);
            ivFav = itemView.findViewById(R.id.iv_fav);
            llSave = itemView.findViewById(R.id.ll_item_save);
            llSave.setOnClickListener(this);
            itemView.setOnClickListener(this);
            tvTitle.setTypeface(typeface);
            tvDate.setTypeface(typeface);
            tvCategory.setTypeface(typeface);
        }

        @Override
        public void onClick(View view) {
            onClick.onCustomItemClick(position,tvCategory.getText().toString(),  view.getId() == R.id.ll_item_save ? OnClick.TYPE_BOOKMARK : OnClick.TYPE_ARTICLE);
        }
    }
}
