package com.editorial.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.adssdk.adapter.NativeAdsListAdapter;
import com.editorial.EditorialSdk;
import com.editorial.R;
import com.editorial.model.ETVocubModel;
import com.editorial.util.EditorialClassUtil;

import java.util.ArrayList;


/**
 * Created by Amit on 1/10/2017.
 */

public class ETWordsAdapter extends NativeAdsListAdapter {

    private final OnClick onClick;
    private ArrayList<ETVocubModel> homeBeen;

    public interface OnClick {
        void onDelete(int position, ETVocubModel item);
    }

    public ETWordsAdapter(ArrayList<ETVocubModel> homeBeen, OnClick activity , Activity activityC) {
        super(activityC , homeBeen, R.layout.ads_native_unified_card, null);
        this.homeBeen = homeBeen;
        this.onClick = (OnClick)activity;
    }

    @Override
    protected RecyclerView.ViewHolder onAbstractCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.et_item_words, parent, false);
        return new ArticleViewHolder(view);
    }

    @Override
    protected void onAbstractBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArticleViewHolder) {
            final ArticleViewHolder viewHolder = (ArticleViewHolder) holder;
            if (homeBeen.size() > position) {
                final ETVocubModel bean = homeBeen.get(position);
                viewHolder.tvWord.setText(bean.getWord());
                viewHolder.tvMeaning.setText(bean.getMeaning());

                if (!TextUtils.isEmpty(bean.getDate())) {
                    viewHolder.tvCount.setText(bean.getDate());
                    viewHolder.tvCount.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.tvCount.setVisibility(View.GONE);
                }

                final int pos = position;
                viewHolder.llDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onClick.onDelete(pos, bean);
                    }
                });

                viewHolder.ivSpeak.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditorialSdk.getInstance().getTextSpeech(viewHolder.itemView.getContext())
                                .speakText(viewHolder.tvMeaning.getText().toString());
                    }
                });
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditorialClassUtil.openTranslatorActivity(viewHolder.itemView.getContext(), bean.getWord());
                    }
                });

            }
        }

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(View itemView) {
            super(itemView);
        }
    }

    class ArticleViewHolder extends ViewHolder {

        private final View ivSpeak;
        private TextView tvWord, tvMeaning, tvCount;
        private LinearLayout llDelete;

        ArticleViewHolder(View itemView) {
            super(itemView);
            tvCount = itemView.findViewById(R.id.item_tv_count);
            tvWord = itemView.findViewById(R.id.item_tv_title);
            tvMeaning = itemView.findViewById(R.id.item_tv_desc);
            ivSpeak = itemView.findViewById(R.id.iv_speak);
            llDelete = itemView.findViewById(R.id.ll_item_delete);
        }

    }
}
