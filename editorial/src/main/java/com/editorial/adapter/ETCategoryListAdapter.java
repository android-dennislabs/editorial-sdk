package com.editorial.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.editorial.EditorialSdk;
import com.editorial.R;
import com.editorial.model.ETCategoryBean;
import com.editorial.util.EditorialUtil;

import java.util.ArrayList;
import java.util.Random;


/**
 * Created by Amit on 1/13/2017.
 */

public class ETCategoryListAdapter extends RecyclerView.Adapter<ETCategoryListAdapter.ViewHolder> {

    private final Typeface typeface;
    private ArrayList<ETCategoryBean> categoryBeen;
    private OnCustomClick onCustomClick;
    private boolean isTextual = false;
    private int layoutRes;

    public void setTextual(boolean textual) {
        isTextual = textual;
    }

    public interface OnCustomClick {
        void onCustomItemClick(int position);
    }

    public ETCategoryListAdapter(Context context, ArrayList<ETCategoryBean> categoryBeen, OnCustomClick onCustomClick, int layoutRes) {
        this.categoryBeen = categoryBeen;
        this.onCustomClick = onCustomClick;
        this.layoutRes = layoutRes;
        this.typeface = EditorialSdk.getInstance().getTypeface(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(layoutRes, parent, false);
        return new ArticleViewHolder(view, onCustomClick);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (holder instanceof ArticleViewHolder) {
            ArticleViewHolder viewHolder = (ArticleViewHolder) holder;
            ETCategoryBean bean = categoryBeen.get(position);
            viewHolder.position = position;
            viewHolder.category.setText(bean.getCategoryName().trim());
            if (isTextual) {
                viewHolder.mainImage.setVisibility(View.GONE);
                viewHolder.tvBack.setVisibility(View.VISIBLE);
                viewHolder.tvBack.setBackgroundColor(Color.parseColor(colors[getRandomNum()]));
                viewHolder.tvBack.setText(getFilterString(bean.getCategoryName()));
            } else {
                if (EditorialUtil.isEmptyOrNull(bean.getImageUrl())) {
                    viewHolder.mainImage.setImageResource(bean.getCategoryImage());
                } else {
                    EditorialSdk.getInstance().getPicasso().load(bean.getImageUrl())
                            .placeholder(R.drawable.exam_place_holder).into(((ArticleViewHolder) holder).mainImage);
                }
            }
        }
    }

    private String getFilterString(String s) {
        String text;
        if (s.contains(" ")) {
            String[] arr = s.split(" ");
            text = arr[0];
        } else {
            text = s;
        }
        s = text.charAt(0) + "";
        return s;
    }

    @Override
    public int getItemCount() {
        return categoryBeen.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(View itemView) {
            super(itemView);
        }
    }

    class ArticleViewHolder extends ViewHolder implements View.OnClickListener {

        TextView category, tvBack;
        ImageView mainImage;
        int position;
        OnCustomClick onClick;

        ArticleViewHolder(View itemView, OnCustomClick onClick) {
            super(itemView);
            this.onClick = onClick;
            category = itemView.findViewById(R.id.item_cat_tv);
            tvBack = itemView.findViewById(R.id.tv_cat_back);
            mainImage = itemView.findViewById(R.id.item_cat_iv);
            itemView.setOnClickListener(this);
            category.setTypeface(typeface);
        }

        @Override
        public void onClick(View view) {
            onClick.onCustomItemClick(position);
        }
    }

    private int getRandomNum() {
        Random r = new Random();
        return r.nextInt(9);
    }

    private String[] colors = {
            "#add68a", "#64c195", "#73bd40", "#03a45e", "#569834", "#00864b", "#417729", "#006c3b", "#7dcf51", "#00c26e"};

}
