# editorial-sdk

#### Library size is : Kb
  
## Setup Project

Add this to your project build.gradle
``` gradle
allprojects {
    repositories {
        maven {
            url "https://jitpack.io"
        }
    }
}
```

Add this to your project build.gradle

#### Dependency
[![](https://jitpack.io/v/org.bitbucket.android-dennislabs/editorial-sdk.svg)](https://jitpack.io/#org.bitbucket.android-dennislabs/editorial-sdk)
```gradle
dependencies {
     implementation 'org.bitbucket.android-dennislabs:editorial-sdk:1.7'
}
```


### Statistics Usage methods
```java
    private EditorialSdk editorialSdk;

    public EditorialSdk getEditorialSdk() {
        if (editorialSdk == null) {
            editorialSdk = EditorialSdk.getInstance()
                    .setAdsSDK(AdsSDK.getInstance())
                    .setDebugMode(BuildConfig.DEBUG)
                    .setConfigManager(getConfigManager())
                    .initUtils(getInstance());
        }
        return editorialSdk;
    }

    public void openEditorial(Context context, int catId, String title, int imageResource, boolean isSubCat , boolean isDate, String host) {
        getEditorialSdk().openCategoryActivity(context, catId, title, imageResource, isSubCat, isDate, host);
    }
```

### EditorialBottomSheetBehaviour Usage methods
```xml
   <?xml version="1.0" encoding="utf-8"?>
   <RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
       xmlns:app="http://schemas.android.com/apk/res-auto"
       android:layout_width="match_parent"
       android:layout_height="match_parent">

       <RelativeLayout
           android:layout_width="match_parent"
           android:layout_height="wrap_content"
           android:layout_alignParentBottom="true"
           android:id="@+id/adViewtop"/>

       <androidx.coordinatorlayout.widget.CoordinatorLayout
           android:layout_above="@id/adViewtop"
           android:layout_width="match_parent"
           android:layout_height="match_parent"
           android:background="@color/themeColorWhiteBlack"
           android:layout_marginBottom="5dp">

           <LinearLayout
               android:layout_width="wrap_content"
               android:layout_height="wrap_content"
               android:orientation="vertical">

               <com.google.android.material.appbar.AppBarLayout
                   android:layout_width="match_parent"
                   android:layout_height="wrap_content"
                   android:theme="@style/ThemeOverlay.AppCompat.Dark.ActionBar">

                   <androidx.appcompat.widget.Toolbar
                       android:id="@+id/toolbar"
                       android:layout_width="match_parent"
                       android:layout_height="?attr/actionBarSize"
                       android:background="?attr/colorPrimary"
                       app:popupTheme="@style/MainActionBar.Popup" />

               </com.google.android.material.appbar.AppBarLayout>

               <include layout="@layout/content_desc" />

               <View
                   android:id="@+id/bottom_sheet_border"
                   android:layout_width="match_parent"
                   android:layout_height="60dp"  />

           </LinearLayout>

           <!-- Adding bottom sheet after main content -->
           <include layout="@layout/et_content_editorial_bottom_sheet" />


       </androidx.coordinatorlayout.widget.CoordinatorLayout>
   </RelativeLayout>
```



### EditorialBottomSheetBehaviour Usage methods
```java
public class DescArticleActivity extends AppCompatActivity{

   private EditorialBottomSheetBehaviour mBottomSheet;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       ...
       initView();
   }

   private void initView() {
       ...
       mBottomSheet = new EditorialBottomSheetBehaviour(this).initUi(getWindow().getDecorView().getRootView());
   }

   @Override
   public void onWordClicked(String word) {
       if (mBottomSheet != null) {
           mBottomSheet.getWordListener().onWordClicked(word);
       }
   }

   @Override
   protected void onStart() {
       super.onStart();
       if (mBottomSheet != null) {
           mBottomSheet.onStart();
       }
   }
   @Override
   protected void onPause() {
       super.onPause();
       if (mBottomSheet != null) {
           mBottomSheet.onPause();
       }
   }

   @Override
   public void onBackPressed() {
       if(mBottomSheet != null && mBottomSheet.isClosed()){
           super.onBackPressed();
       }
   }
}
```