package com.sample.editorialsdk;

import android.app.Activity;
import android.content.Context;

import com.adssdk.AdsSDK;
import com.config.config.ConfigManager;
import com.config.util.ConfigUtil;
import com.editorial.EditorialSdk;
import com.helper.application.BaseApplication;
import com.helper.model.CategoryProperty;

public class AppApplication extends BaseApplication {
    @Override
    public void initLibs() {

    }

    private static AppApplication _instance;
    private ConfigManager configManager;

    public static AppApplication getInstance() {
        return _instance;
    }

    @Override
    public boolean isDebugMode() {
        return BuildConfig.DEBUG;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        _instance = this;
        configManager = getConfigManager();
    }

    public void loadConfig() {
        if (configManager != null) {
            configManager.loadConfig();
        }
    }

    public ConfigManager getConfigManager() {
        if(configManager == null){
            configManager = ConfigManager.getInstance(this, ConfigUtil.getSecurityCode(this), BuildConfig.DEBUG);
        }
        return configManager;
    }

    private EditorialSdk editorialSdk;

    public EditorialSdk getEditorialSdk() {
        if (editorialSdk == null) {
            editorialSdk = EditorialSdk.getInstance()
                    .setAdsSDK(AdsSDK.getInstance())
                    .setDebugMode(BuildConfig.DEBUG)
                    .setConfigManager(getConfigManager())
                    .initUtils(getInstance());
        }
        return editorialSdk;
    }

    public void openEditorial(Context context, int catId, String title, int imageResource, boolean isSubCat , boolean isDate, String host) {
        getEditorialSdk().openCategoryActivity(context, catId, title, imageResource, isSubCat, isDate, host);
    }

    public void openEditorial(Activity activity, CategoryProperty property) {
        getEditorialSdk()
                .openCategoryActivity(activity, property.getCatId()
                        , property.getTitle(), property.getImageResId()
                        , property.isSubCat(), true, property.getHost());
    }

}
