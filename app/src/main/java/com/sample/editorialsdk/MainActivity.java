package com.sample.editorialsdk;

import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SwitchCompat;

import com.config.config.ConfigConstant;
import com.helper.model.CategoryProperty;
import com.helper.util.DayNightPreference;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        callConfig();

        initViews();
    }

    private void initViews() {
        (findViewById(R.id.btn_open)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppApplication.getInstance().openEditorial(MainActivity.this, getCategoryProperty());
            }
        });

        SwitchCompat switchCompat = findViewById(R.id.switchCompat);
        switchCompat.setChecked(DayNightPreference.isNightModeEnabled(this));

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                DayNightPreference.setNightMode(MainActivity.this, isChecked);
            }
        });
    }

    private CategoryProperty getCategoryProperty() {
        CategoryProperty property = new CategoryProperty();
        property.setCatId(819);
        property.setTitle("Editorial");
        property.setHost(ConfigConstant.HOST_TRANSLATOR);
        property.setImageResId(R.drawable.ic_editorial_default);
        property.setDate(true);
        return property;
    }

    private void callConfig() {
        if (AppApplication.getInstance() != null &&
                (AppApplication.getInstance().getConfigManager() == null
                        || !AppApplication.getInstance().getConfigManager().isConfigLoaded())) {
            showConnectingTextView("NetworkConfig.CONNECTING");
            AppApplication.getInstance().loadConfig();
        } else if (AppApplication.getInstance() != null && AppApplication.getInstance().getConfigManager() != null
                && AppApplication.getInstance().getConfigManager().isConfigLoaded())
            showConnectingTextView("NetworkConfig.CONNECTED");
    }

    private void showConnectingTextView(String connecting) {

    }


}
